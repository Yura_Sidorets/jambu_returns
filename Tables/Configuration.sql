CREATE TABLE [dbo].[ConfigurationField] (
    [MerchantConfigId]    INT            NOT NULL,
    [Email]               NVARCHAR (50)  NOT NULL,
    [Password]            NVARCHAR (500) NOT NULL,
    [Country]             NVARCHAR (50)  NOT NULL,
    [State]               NVARCHAR (50)  NOT NULL,
    [City]                NVARCHAR (50)  NOT NULL,
    [Address1]            NVARCHAR (50)  NOT NULL,
    [Address2]            NVARCHAR (50)  NULL,
    [Zip]                 NCHAR (10)     NOT NULL,
    [Telephone]           NVARCHAR (50)  NULL,
    [DailyMail]           NVARCHAR (50)  NOT NULL,
    [AccessLicenseNumber] VARCHAR (50)   NULL,
    [AccountNumber]       VARCHAR (50)   NULL,
    [AccountName]         VARCHAR (50)   NULL
);

