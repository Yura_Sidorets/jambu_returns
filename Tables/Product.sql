CREATE TABLE [dbo].[Product] (
    [ProductId] INT            NOT NULL,
    [Name]      VARCHAR (100)   NULL,
    [Size]      VARCHAR (100)   NULL,
    [Color]     VARCHAR (100)   NULL,
    [Url]       NVARCHAR (MAX) NULL,
    [Price]     VARCHAR (100)   NULL,
    [Weight]    VARCHAR (100)   NULL,
    [Width]     VARCHAR (100)   NULL,
    [Height]    VARCHAR (100)   NULL,
    [Depth]     VARCHAR (100)   NULL,
    [SKU]       VARCHAR (100)   NULL
);

