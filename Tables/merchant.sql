CREATE TABLE [dbo].[Merchant] (
    [MerchantID]      INT           NOT NULL,
    [AccessToken]     NVARCHAR (50) NOT NULL,
    [WebApiClientKey] NVARCHAR (50) NULL,
    [WebsiteId]       NVARCHAR (50) NULL,
    [StoreHash]       NVARCHAR (50) NOT NULL,
    [Hash]            NVARCHAR (50) NULL,
    [Configured]      BIT           NOT NULL
);

