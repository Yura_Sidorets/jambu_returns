CREATE TABLE [dbo].[Return] (
    [ReturnID]       INT            IDENTITY (1, 1) NOT NULL,
    [ProductID]      INT            NOT NULL,
    [OrderID]        INT            NOT NULL,
    [Customer]       VARCHAR (50)   NOT NULL,
    [Date]           VARCHAR (50)   NOT NULL,
    [Status]         VARCHAR (10)   DEFAULT ('Approved') NOT NULL,
    [Comment]        NVARCHAR (300) NOT NULL,
    [Reason]         VARCHAR (50)   NOT NULL,
    [Action]         VARCHAR (50)   NOT NULL,
    [Notes]          VARCHAR (MAX)  NULL,
    [Quantity]       INT            NOT NULL,
    [ProductName]    VARCHAR (100)  NULL,
    [TrackingNumber] VARCHAR (MAX)  NULL,
    [ShippingLabel]  VARCHAR (MAX)  NULL
);

