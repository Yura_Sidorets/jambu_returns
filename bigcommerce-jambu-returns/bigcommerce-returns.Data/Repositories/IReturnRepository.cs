﻿using System.Collections.Generic;
using bigcommerce_returns.Data;

namespace bigcommerce_returns.Repositories
{
    public interface IReturnRepository
    {
        void Add(Return returnOrder);

        Return GetByReturnId(string returnId);

        Return GetBySku(string sku);

        void Remove(string returnId);

        void UpdateNotes(int returnId, string note);

        IEnumerable<Return> GetAll();

        IEnumerable<Return> Search(string pattern, int? offset, int next);

        void UpdateStatus(int returnId, string status);

        void CompleteReturn(int returnId);

        void AddProduct(Product product);

        Product GetProduct(int productId);

        IEnumerable<Return> GetAllApproved();

        IEnumerable<Return> GetReturnByOrder(int orderId);

        void UpdateProduct(Product product);

        Product GetProductBySku(string sku);

        Return GetReturnBySku(string sku);

        Product GetProductByName(string name);

        string GetOrderIdMapping(string orderId);

        IEnumerable<Return> GetReturnsWithPagination(int? offset, int next);

        int GetReturnsCount();

        int GetSearchResultsCount(string pattern);

        IEnumerable<Return> GetAllReturnsByDate(string start, string end);

    }
}
