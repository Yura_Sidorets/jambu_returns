﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using bigcommerce_returns.Data;
using Dapper;

namespace bigcommerce_returns.Repositories
{
    public class MerchantRepository : IMerchantRepository
    {
        private readonly string _connectionString;
        private static string _signedPayload;

        public MerchantRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void SetSignedPayload(string payload)
        {
            _signedPayload = payload;
        }

        public string GetSignedPayload()
        {
            return _signedPayload;
        }

        public void Add(Merchant merchant)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute($"INSERT INTO Merchant (MerchantID, AccessToken, StoreHash, Configured, WebApiClientKey, SignedPayload) VALUES('{merchant.Id}', '{merchant.AccessToken}', '{merchant.StoreHash}', 0, '{merchant.WebApiClientKey}', '{merchant.SignedPayload}')");
            }
        }

        public void Configure(string merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET Configured = 1 WHERE MerchantId = @MerchantId", new { MerchantId = merchantId });
            }
        }

        public Merchant Get(string merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Merchant>("SELECT MerchantID, AccessToken, WebApiClientKey, WebsiteId, StoreHash, Hash, Configured IsConfigured FROM Merchant Where MerchantID = @MerchantId", new { MerchantId = merchantId }).FirstOrDefault();
            }
        }

        public Merchant GetMerchantByStoreHash(string storeHash)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Merchant>("SELECT MerchantID, AccessToken, WebApiClientKey, WebsiteId, StoreHash, Hash, Configured IsConfigured FROM Merchant Where StoreHash = @StoreHash", new { StoreHash = storeHash }).FirstOrDefault();
            }
        }

        public string GetMerchantHashById(string merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<string>("SELECT Hash FROM Merchant Where MerchantId = @MerchantId", new { MerchantId = merchantId }).FirstOrDefault();
            }
        }

        public string GetMerchantIdByHash(string hash)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<string>("SELECT MerchantID FROM Merchant Where Hash = @Hash", new { Hash = hash }).FirstOrDefault();
            }
        }

        public Merchant GetMerchantByClientKey(string clientKey)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Merchant>("SELECT * FROM Merchant Where WebApiClientKey = @ClientKey", new { ClientKey = clientKey }).FirstOrDefault();
            }
        }

        public void Remove(string merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET Configured = 0 WHERE MerchantId = @MerchantId", new { MerchantId = merchantId });
            }
        }

        public void UpdateSignedPayload(string clientKey, string signedPayload)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET SignedPayload = @SignedPayload WHERE WebApiClientKey = @ClientKey", new { SignedPayload = signedPayload, ClientKey = clientKey });
            }
        }

        public void UpdateAccessToken(int merchantId, string accessToken)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE Merchant SET AccessToken = @AccessToken WHERE MerchantId = @MerchantId", new { AccessToken = accessToken, MerchantId = merchantId });
            }
        }
    }
}
