﻿using bigcommerce_returns.Data;

namespace bigcommerce_returns.Repositories
{
    public interface IMerchantRepository
    {
        void Add(Merchant merchant);
        Merchant Get(string merchantId);
        void Configure(string merchantId);

        void Remove(string merchantId);

        string GetMerchantIdByHash(string hash);

        string GetMerchantHashById(string merchantId);

        void UpdateSignedPayload(string clientKey, string signedPayload);

        Merchant GetMerchantByClientKey(string clientKey);

        Merchant GetMerchantByStoreHash(string storeHash);

        void UpdateAccessToken(int merchantId, string accessToken);

        void SetSignedPayload(string payload);

        string GetSignedPayload();
    }
}