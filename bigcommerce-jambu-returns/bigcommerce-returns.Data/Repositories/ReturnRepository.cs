﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using bigcommerce_returns.Data;
using Dapper;

namespace bigcommerce_returns.Repositories
{
    public class ReturnRepository : IReturnRepository
    {
        private readonly string _connectionString;

        public ReturnRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(Return returnOrder)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                returnOrder.Customer = returnOrder.Customer.Replace("'", "\"");
                returnOrder.Comment = returnOrder.Comment.Replace("'", "\"");

                connection.Execute($"INSERT INTO [Return] (ProductID, OrderID, Customer, Date, Status, Comment, Reason, Action, Notes, Quantity, ProductName, TrackingNumber, ShippingLabel, SKU) VALUES('{returnOrder.ProductId}', '{returnOrder.OrderId}', '{returnOrder.Customer}', '{returnOrder.Date}', '{returnOrder.Status}', '{returnOrder.Comment}', '{returnOrder.Reason}', '{returnOrder.Action}', '{returnOrder.Notes}', '{returnOrder.Quantity}', '{returnOrder.ProductName}', '{returnOrder.TrackingNumber}', '{returnOrder.ShippingLabel}', '{returnOrder.SKU}')");
            }
        }

        public Return Get(string productId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Return>("SELECT * FROM [Return] Where ProductID = @ProductId", new { ProductId = productId }).FirstOrDefault();
            }
        }


        public Return GetBySku(string sku)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Return>("SELECT * FROM [Return] Where SKU = @Sku", new { Sku = sku}).FirstOrDefault();
            }
        }


        public Return GetByReturnId(string returnId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Return>("SELECT * FROM [Return] Where ReturnID = @ReturnId", new { ReturnId = returnId }).FirstOrDefault();
            }
        }

        public IEnumerable<Return> GetAll()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Return>("SELECT * FROM [Return]");
            }
        }

        public IEnumerable<Return> GetAllApproved()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                string actualDate = DateTime.Now.AddDays(-1).ToShortDateString();
                //var date = DateTime.Now.AddDays(-31).ToShortDateString().Split('.');
                //if (date.Length > 0)
                //{
                //    actualDate = date[1].Trim('0') + '/' + date[0].Trim('0') + '/' + date[2];
                //}
               return connection.Query<Return>($"SELECT * FROM [Return] Where Status LIKE '%Completed%' AND DateCompleted = '{actualDate}'");
            }
        }

        public void Remove(string returnId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("DELETE FROM [Return] WHERE ReturnId = @returnId", new { ReturnId = returnId });
            }
        }

        public void UpdateNotes(int returnId, string note)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE [Return] SET Notes = @Note WHERE ReturnId = @ReturnId", new { Note = note, ReturnId = returnId });
            }
        }

        public void UpdateStatus(int returnId, string status)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE [Return] SET Status = @Status WHERE ReturnId = @ReturnId", new { Status = status, ReturnId = returnId });
            }
        }

        public void CompleteReturn(int returnId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute("UPDATE [Return] SET Status = @Status, DateCompleted = @Date WHERE ReturnId = @ReturnId", new { Status = "Completed", ReturnId = returnId, Date = DateTime.Today.ToShortDateString()});
            }
        }

        public void AddProduct(Product product)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute($"INSERT INTO Product ( ProductId, Name, Size, Color, Url, Price, Weight, Width, Height, Depth, SKU) VALUES ('{product.ProductId}', '{product.Name}', '{product.Size}', '{product.Color}','{product.Url}', '{product.Price}', '{product.Weight}', '{product.Width}','{product.Height}', '{product.Depth}', '{product.Sku}') SELECT '{product.Sku}' WHERE NOT EXISTS(SELECT * FROM Product WHERE SKU = '{product.Sku}')");
            }
        }

        public Product GetProduct(int productId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Product>("SELECT * FROM Product  Where ProductId = @ProductId", new { ProductId = productId }).FirstOrDefault();
            }
        }

        public void UpdateProduct(Product product)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute($"UPDATE Product SET Name = '{product.Name}', Size = '{product.Size}', Color = '{product.Color}', Url = '{product.Url}' , Price = '{product.Price}', Weight = '{product.Weight}', Width = '{product.Width}', Height = '{product.Height}', Depth = '{product.Depth}', SKU = '{product.Sku}' WHERE SKU = @SKU", new { SKU = product.Sku });
            }
        }

        public IEnumerable<Return> GetReturnByOrder(int orderId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Return>("SELECT * FROM [Return] Where OrderId = @OrderId", new { OrderId = orderId });
            }
        }

        public Return GetReturnBySku(string sku)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Return>("SELECT * FROM [Return] Where SKU = @Sku", new { Sku = sku }).FirstOrDefault();
            }
        }

        public Product GetProductBySku(string sku)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Product>("SELECT * FROM Product Where SKU = @Sku", new { Sku = sku }).FirstOrDefault();
            }
        }

        public Product GetProductByName(string name)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Product>("SELECT * FROM Product Where Name = @Name", new { Name = name }).FirstOrDefault();
            }
        }

        public string GetOrderIdMapping(string orderId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<string>("SELECT BCOrderID FROM OrdersMap Where JambuOrderID = @JambuOrderID", new { JambuOrderID = orderId }).FirstOrDefault();
            }
        }

        public IEnumerable<Return> Search(string pattern, int? offset, int next)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                pattern = pattern.Replace("'", " ");
                return connection.Query<Return>("SELECT * FROM [Return] " +
                                                "JOIN Product ON [Return].[SKU] = Product.[SKU] " + 
                                                $"Where (ReturnID LIKE '%{pattern}%' OR " +
                                                $"[Return].[ProductID] LIKE '%{pattern}%' OR " +
                                                $"[Return].OrderID LIKE '%{pattern}%' OR " +
                                                $"[Return].Customer LIKE '%{pattern}%' OR " +
                                                $"[Return].Date LIKE '%{pattern}%' OR " +
                                                $"[Return].Status LIKE '%{pattern}%' OR " +
                                                $"[Return].Comment LIKE '%{pattern}%' OR " +
                                                $"[Return].Reason LIKE '%{pattern}%' OR " +
                                                $"[Return].Action LIKE '%{pattern}%' OR " +
                                                $"[Return].Notes LIKE '%{pattern}%' OR " +
                                                $"[Return].Quantity LIKE '%{pattern}%' OR " +
                                                $"[Return].ProductName LIKE '%{pattern}%') "+
                                                "ORDER BY ReturnID DESC OFFSET @Offset ROWS FETCH NEXT @Next ROWS ONLY ", new { Offset = offset, Next = next });
            }
        }


        public IEnumerable<Return> GetReturnsWithPagination(int? offset, int next)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Return>("SELECT * FROM [Return] " +
                                                "JOIN Product ON [Return].[SKU] = Product.[SKU] " +
                                                "ORDER BY ReturnID DESC OFFSET @Offset ROWS FETCH NEXT @Next ROWS ONLY ",
                    new { Offset = offset, Next = next });
            }
        }

        public int GetReturnsCount()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                return connection.Query<int>("SELECT count(*) FROM [Return]").FirstOrDefault();
            }
        }

        public int GetSearchResultsCount(string pattern)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                pattern = pattern.Replace("'", " ");
                return connection.Query<int>("SELECT count(*) FROM [Return] " +
                                                "JOIN Product ON [Return].[SKU] = Product.[SKU] " +
                                                $"Where (ReturnID LIKE '%{pattern}%' OR " +
                                                $"[Return].[ProductID] LIKE '%{pattern}%' OR " +
                                                $"[Return].OrderID LIKE '%{pattern}%' OR " +
                                                $"[Return].Customer LIKE '%{pattern}%' OR " +
                                                $"[Return].Date LIKE '%{pattern}%' OR " +
                                                $"[Return].Status LIKE '%{pattern}%' OR " +
                                                $"[Return].Comment LIKE '%{pattern}%' OR " +
                                                $"[Return].Reason LIKE '%{pattern}%' OR " +
                                                $"[Return].Action LIKE '%{pattern}%' OR " +
                                                $"[Return].Notes LIKE '%{pattern}%' OR " +
                                                $"[Return].Quantity LIKE '%{pattern}%' OR " +
                                                $"[Return].ProductName LIKE '%{pattern}%') ").FirstOrDefault();
            }

        }

        public IEnumerable<Return> GetAllReturnsByDate(string start, string end)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                
                return connection.Query<Return>("SELECT  * FROM [Return] WHERE CONVERT(DATE, NULLIF(Date, RTRIM(LTRIM(''))), 101) < CAST(@EndDate As Date) AND Date IS NOT NULL",
                    new {StartDate = start, EndDate = end});
            }
        }
    }
}