﻿using bigcommerce_returns.Data;

namespace bigcommerce_returns.Repositories
{
    public interface IConfigurationRepository
    {
        ApplicationConfiguration GetApplicationConfiguration(int merchantId);
        void SaveApplicationConfiguration(int merchantId, ApplicationConfiguration config);
        void UpdateApplicationConfiguration(int merchantId, ApplicationConfiguration config);
    }
}
