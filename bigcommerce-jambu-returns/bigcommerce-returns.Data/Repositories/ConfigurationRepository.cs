﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using bigcommerce_returns.Data;
using Dapper;

namespace bigcommerce_returns.Repositories
{
    public class ConfigurationRepository : IConfigurationRepository
    {
        private string _connectionString;

        public ConfigurationRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ApplicationConfiguration GetApplicationConfiguration(int merchantId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                ApplicationConfiguration appConf = new ApplicationConfiguration();
                
                appConf.UpsCredentials = connection.Query<UpsCredentials>(
                    @"select Email, Password, AccessLicenseNumber, AccountNumber from ConfigurationField
                      where MerchantConfigId = @MerchantId",
                    new {MerchantId = merchantId}).FirstOrDefault();
                appConf.Address = connection.Query<Address>(
                    @"select Country, State, City, Address1, Address2, Zip, Telephone, AccountName from ConfigurationField
                      where MerchantConfigId = @MerchantId",
                    new { MerchantId = merchantId }).FirstOrDefault();
                appConf.FtpCredentials = connection.Query<FtpCredentials>(@"select FtpUsername, FtpPassword, FtpPath, FtpUrl from ConfigurationField
                      where MerchantConfigId = @MerchantId",
                    new { MerchantId = merchantId }).FirstOrDefault();

                return appConf;
            }
        }

        public void SaveApplicationConfiguration(int merchantId, ApplicationConfiguration config)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute(
                    $"INSERT INTO ConfigurationField  (MerchantConfigId, Email, Password, Country, State, City, Address1, Address2, Zip, Telephone, FtpUsername, FtpPassword, FtpPath, FtpUrl, AccessLicenseNumber, AccountNumber, AccountName) VALUES (@MerchantID, '{config.UpsCredentials.Email}', '{config.UpsCredentials.Password}', '{config.Address.Country}',  '{config.Address.State}',  '{config.Address.City}',  '{config.Address.Address1}',  '{config.Address.Address2}',  '{config.Address.Zip}',  '{config.Address.Telephone}',  '{config.FtpCredentials.FtpUsername}', '{config.FtpCredentials.FtpPassword}', '{config.FtpCredentials.FtpPath}', '{config.FtpCredentials.FtpUrl}', '{config.UpsCredentials.AccessLicenseNumber}', '{config.UpsCredentials.AccountNumber}', '{config.Address.AccountName}')",
                    new { MerchantId = merchantId});
            }
        }

        public void UpdateApplicationConfiguration(int merchantId, ApplicationConfiguration config)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Execute(
                    $"Update ConfigurationField Set MerchantConfigId = @MerchantID, Email = '{config.UpsCredentials.Email}', Password = '{config.UpsCredentials.Password}', Country = '{config.Address.Country}', State =  '{config.Address.State}', City =  '{config.Address.City}', Address1 = '{config.Address.Address1}', Address2 = '{config.Address.Address2}', Zip = '{config.Address.Zip}', Telephone = '{config.Address.Telephone}', FtpUsername = '{config.FtpCredentials.FtpUsername}', FtpPassword = '{config.FtpCredentials.FtpPassword}', FtpPath = '{config.FtpCredentials.FtpPath}', FtpUrl = '{config.FtpCredentials.FtpUrl}', AccessLicenseNumber = '{config.UpsCredentials.AccessLicenseNumber}', AccountNumber = '{config.UpsCredentials.AccountNumber}', AccountName = '{config.Address.AccountName}'",
                    new { MerchantId = merchantId });
            }
        }
    }
}
