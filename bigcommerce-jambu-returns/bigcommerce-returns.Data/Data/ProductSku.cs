﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace bigcommerce_returns.Data
{
    public class SkuOption
    {

        [JsonProperty("product_option_id")]
        public int product_option_id { get; set; }

        [JsonProperty("option_value_id")]
        public int option_value_id { get; set; }
    }

    public class ProductSku
    {

        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("product_id")]
        public int product_id { get; set; }

        [JsonProperty("sku")]
        public string sku { get; set; }

        [JsonProperty("price")]
        public object price { get; set; }

        [JsonProperty("adjusted_price")]
        public string adjusted_price { get; set; }

        [JsonProperty("cost_price")]
        public string cost_price { get; set; }

        [JsonProperty("upc")]
        public string upc { get; set; }

        [JsonProperty("inventory_level")]
        public int inventory_level { get; set; }

        [JsonProperty("inventory_warning_level")]
        public int inventory_warning_level { get; set; }

        [JsonProperty("bin_picking_number")]
        public string bin_picking_number { get; set; }

        [JsonProperty("weight")]
        public object weight { get; set; }

        [JsonProperty("adjusted_weight")]
        public string adjusted_weight { get; set; }

        [JsonProperty("is_purchasing_disabled")]
        public bool is_purchasing_disabled { get; set; }

        [JsonProperty("purchasing_disabled_message")]
        public string purchasing_disabled_message { get; set; }

        [JsonProperty("image_file")]
        public string image_file { get; set; }

        [JsonProperty("options")]
        public IList<SkuOption> options { get; set; }
    }


}
