﻿using Newtonsoft.Json;

namespace bigcommerce_returns.Data
{
    public class Order
    {
        public int Id { get; set; }
        [JsonProperty("customer_id")]
        public int CustomerId { get; set; }
    }
}
