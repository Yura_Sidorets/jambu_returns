﻿namespace bigcommerce_returns.Data
{
    public class ApplicationConfiguration
    {
        public UpsCredentials UpsCredentials { get; set; }
        public Address Address { get; set; }
        public FtpCredentials FtpCredentials { get; set; }
    }

    public class UpsCredentials
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string AccessLicenseNumber { get; set; }
        public string AccountNumber { get; set; }
    }

    public class Address
    {
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Zip { get; set; }
        public string Telephone { get; set; }
        public string AccountName { get; set; }
    }

    public class FtpCredentials
    {
        public string FtpUsername { get; set; }
        public string FtpPassword { get; set; }
        public string FtpUrl { get; set; }
        public string FtpPath { get; set; }
    }
}
