﻿using Newtonsoft.Json;

namespace bigcommerce_returns.Data
{
    public class Addresses
    {

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("resource")]
        public string Resource { get; set; }
    }

    public class Customer
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("form_fields")]
        public object FormFields { get; set; }

        [JsonProperty("date_created")]
        public string DateCreated { get; set; }

        [JsonProperty("date_modified")]
        public string DateModified { get; set; }

        [JsonProperty("store_credit")]
        public string StoreCredit { get; set; }

        [JsonProperty("registration_ip_address")]
        public string RegistrationIpAddress { get; set; }

        [JsonProperty("customer_group_id")]
        public int CustomerGroupId { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("tax_exempt_category")]
        public string TaxExemptCategory { get; set; }

        [JsonProperty("reset_pass_on_login")]
        public bool ResetPassOnLogin { get; set; }

        [JsonProperty("accepts_marketing")]
        public bool AcceptsMarketing { get; set; }

        [JsonProperty("addresses")]
        public Addresses Addresses { get; set; }
    }



}
