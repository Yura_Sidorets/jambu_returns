﻿using Newtonsoft.Json;

namespace bigcommerce_returns.Data
{
    public class ProductImage
    {

        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("product_id")]
        public int product_id { get; set; }

        [JsonProperty("image_file")]
        public string image_file { get; set; }

        [JsonProperty("zoom_url")]
        public string zoom_url { get; set; }

        [JsonProperty("thumbnail_url")]
        public string thumbnail_url { get; set; }

        [JsonProperty("standard_url")]
        public string standard_url { get; set; }

        [JsonProperty("tiny_url")]
        public string tiny_url { get; set; }

        [JsonProperty("is_thumbnail")]
        public bool is_thumbnail { get; set; }

        [JsonProperty("sort_order")]
        public int sort_order { get; set; }

        [JsonProperty("description")]
        public object description { get; set; }

        [JsonProperty("date_created")]
        public string date_created { get; set; }
    }

}
