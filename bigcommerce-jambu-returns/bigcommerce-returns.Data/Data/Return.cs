﻿using Newtonsoft.Json;

namespace bigcommerce_returns.Data
{
    public class Return
    {
        public int? ReturnId { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public string Customer { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public string Reason { get; set; }
        public string Action { get; set; }
        public string Notes { get; set; }
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public string TrackingNumber { get; set; }
        public string ShippingLabel { get; set; }
        public string SKU {get;set;}

        public string ShippingCost { get; set; }
        public string UPC { get; set; }
        public string SalesPrice { get; set; }
        public string Tax { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("custom_url")]
        public string Url { get; set; }

        public string Size { get; set; }

        public string Color { get; set; }

        public string Sku { get; set; }

        [JsonProperty("weight")]
        public string Weight { get; set; }

        [JsonProperty("width")]
        public string Width { get; set; }

        [JsonProperty("height")]
        public string Height { get; set; }

        [JsonProperty("depth")]
        public string Depth { get; set; }

        [JsonProperty("sale_price")]
        public string SalePrice { get; set; }

        [JsonProperty("skus")]
        public Product.ProdSkus Skus { get; set; }

        public class ProdSkus
        {

            [JsonProperty("url")]
            public string url { get; set; }

            [JsonProperty("resource")]
            public string resource { get; set; }
        }
    }
}