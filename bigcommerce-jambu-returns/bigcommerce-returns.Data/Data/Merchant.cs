﻿namespace bigcommerce_returns.Data
{
    public class Merchant
    {
        public string Id { get; set; }
        public string AccessToken { get; set; }
        public string WebApiClientKey { get; set; }
        public bool IsConfigured { get; set; }
        public string WebsiteId { get; set; }
        public string StoreHash { get; set; }
        public string SignedPayload { get; set; }
    }
}