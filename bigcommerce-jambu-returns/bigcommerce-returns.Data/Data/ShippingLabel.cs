﻿namespace bigcommerce_returns.Data
{
    public class ShippingLabel
    {
        public string Label { get; set; }

        public ShippingLabel()
        {
            Label = "";
        }
    }
}
