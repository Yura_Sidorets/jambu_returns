﻿namespace bigcommerce_returns.Data
{
    public class Store
    {
        public User User { get; set; }
        public Owner Owner { get; set; }
        public string Context { get; set; }
        public string StoreHash { get; set; }
        public string Timestamp { get; set; }
    }
}