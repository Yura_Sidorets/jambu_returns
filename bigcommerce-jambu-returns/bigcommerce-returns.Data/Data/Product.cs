﻿using Newtonsoft.Json;

namespace bigcommerce_returns.Data
{
    public class Product
    {
        [JsonProperty("id")]
        public int ProductId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("custom_url")]
        public string Url { get; set; }

        public string Size { get; set; }
        
        public string Color { get; set; }

        public string Sku { get; set; }

        [JsonProperty("weight")]
        public string Weight { get; set; }

        [JsonProperty("width")]
        public string Width { get; set; }

        [JsonProperty("height")]
        public string Height { get; set; }

        [JsonProperty("depth")]
        public string Depth { get; set; }

        [JsonProperty("sale_price")]
        public string SalePrice { get; set; }

        [JsonProperty("upc")]
        public string UPC { get; set; }

        [JsonProperty("skus")]
        public ProdSkus Skus { get; set; }

        [JsonProperty("primary_image")]
        public PrimaryImage Primary_image { get; set; }

        public class PrimaryImage
        {

            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("zoom_url")]
            public string ZoomUrl { get; set; }

            [JsonProperty("thumbnail_url")]
            public string ThumbnailUrl { get; set; }

            [JsonProperty("standard_url")]
            public string StandardUrl { get; set; }

            [JsonProperty("tiny_url")]
            public string TinyUrl { get; set; }
        }

        public class ProdSkus
        {

            [JsonProperty("url")]
            public string url { get; set; }

            [JsonProperty("resource")]
            public string resource { get; set; }
        }
    }


}
