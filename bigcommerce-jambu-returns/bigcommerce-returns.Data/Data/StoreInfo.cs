﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace bigcommerce_returns.Data
{
    public class DateFormat
    {

        [JsonProperty("display")]
        public string Display { get; set; }

        [JsonProperty("export")]
        public string Export { get; set; }

        [JsonProperty("extended_display")]
        public string ExtendedDisplay { get; set; }
    }

    public class Timezone
    {

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("raw_offset")]
        public int RawOffset { get; set; }

        [JsonProperty("dst_offset")]
        public int DstOffset { get; set; }

        [JsonProperty("dst_correction")]
        public bool DstCorrection { get; set; }

        [JsonProperty("date_format")]
        public DateFormat DateFormat { get; set; }
    }

    public class Features
    {

        [JsonProperty("stencil_enabled")]
        public bool StencilEnabled { get; set; }
    }

    public class StoreInfo
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("domain")]
        public string Domain { get; set; }

        [JsonProperty("secure_url")]
        public string SecureUrl { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("admin_email")]
        public string AdminEmail { get; set; }

        [JsonProperty("order_email")]
        public string OrderEmail { get; set; }

        [JsonProperty("timezone")]
        public Timezone Timezone { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("currency_symbol")]
        public string CurrencySymbol { get; set; }

        [JsonProperty("decimal_separator")]
        public string DecimalSeparator { get; set; }

        [JsonProperty("thousands_separator")]
        public string ThousandsSeparator { get; set; }

        [JsonProperty("decimal_places")]
        public int DecimalPlaces { get; set; }

        [JsonProperty("currency_symbol_location")]
        public string CurrencySymbolLocation { get; set; }

        [JsonProperty("weight_units")]
        public string WeightUnits { get; set; }

        [JsonProperty("dimension_units")]
        public string DimensionUnits { get; set; }

        [JsonProperty("dimension_decimal_places")]
        public string DimensionDecimalPlaces { get; set; }

        [JsonProperty("dimension_decimal_token")]
        public string DimensionDecimalToken { get; set; }

        [JsonProperty("dimension_thousands_token")]
        public string DimensionThousandsToken { get; set; }

        [JsonProperty("plan_name")]
        public string PlanName { get; set; }

        [JsonProperty("plan_level")]
        public string PlanLevel { get; set; }

        [JsonProperty("industry")]
        public string Industry { get; set; }

        [JsonProperty("is_price_entered_with_tax")]
        public bool IsPriceEnteredWithTax { get; set; }

        [JsonProperty("active_comparison_modules")]
        public IList<object> ActiveComparisonModules { get; set; }

        [JsonProperty("features")]
        public Features Features { get; set; }
    }


}
