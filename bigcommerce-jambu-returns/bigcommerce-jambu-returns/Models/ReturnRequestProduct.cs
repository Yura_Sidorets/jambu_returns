﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{
    public class ProductOption
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("option_id")]
        public int OptionId { get; set; }

        [JsonProperty("order_product_id")]
        public int OrderProductId { get; set; }

        [JsonProperty("product_option_id")]
        public int ProductOptionId { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("display_value")]
        public string DisplayValue { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("display_style")]
        public string DisplayStyle { get; set; }
    }

    public class ReturnRequestProduct
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("order_address_id")]
        public int OrderAddressId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("base_price")]
        public string BasePrice { get; set; }

        [JsonProperty("price_ex_tax")]
        public string PriceExTax { get; set; }

        [JsonProperty("price_inc_tax")]
        public string PriceIncTax { get; set; }

        [JsonProperty("price_tax")]
        public string PriceTax { get; set; }

        [JsonProperty("base_total")]
        public string BaseTotal { get; set; }

        [JsonProperty("total_ex_tax")]
        public string TotalExTax { get; set; }

        [JsonProperty("total_inc_tax")]
        public string TotalIncTax { get; set; }

        [JsonProperty("width")]
        public string Width { get; set; }

        [JsonProperty("height")]
        public string Height { get; set; }

        [JsonProperty("depth")]
        public string Depth { get; set; }

        [JsonProperty("weight")]
        public string Weight { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("base_cost_price")]
        public string BaseCostPrice { get; set; }

        [JsonProperty("cost_price_inc_tax")]
        public string CostPriceIncTax { get; set; }

        [JsonProperty("cost_price_ex_tax")]
        public string CostPriceExTax { get; set; }

        [JsonProperty("cost_price_tax")]
        public string CostPriceTax { get; set; }

        [JsonProperty("is_refunded")]
        public bool IsRefunded { get; set; }

        [JsonProperty("quantity_refunded")]
        public int QuantityRefunded { get; set; }

        [JsonProperty("refund_amount")]
        public string RefundAmount { get; set; }

        [JsonProperty("return_id")]
        public int ReturnId { get; set; }

        [JsonProperty("wrapping_name")]
        public string WrappingName { get; set; }

        [JsonProperty("base_wrapping_cost")]
        public string BaseWrappingCost { get; set; }

        [JsonProperty("wrapping_cost_ex_tax")]
        public string WrappingCostExTax { get; set; }

        [JsonProperty("wrapping_cost_inc_tax")]
        public string WrappingCostIncTax { get; set; }

        [JsonProperty("wrapping_cost_tax")]
        public string WrappingCostTax { get; set; }

        [JsonProperty("wrapping_message")]
        public string WrappingMessage { get; set; }

        [JsonProperty("quantity_shipped")]
        public int QuantityShipped { get; set; }

        [JsonProperty("event_name")]
        public object EventName { get; set; }

        [JsonProperty("event_date")]
        public string EventDate { get; set; }

        [JsonProperty("fixed_shipping_cost")]
        public string FixedShippingCost { get; set; }

        [JsonProperty("ebay_item_id")]
        public string EbayItemId { get; set; }

        [JsonProperty("ebay_transaction_id")]
        public string EbayTransactionId { get; set; }

        [JsonProperty("option_set_id")]
        public int? OptionSetId { get; set; }

        [JsonProperty("parent_order_product_id")]
        public object ParentOrderProductId { get; set; }

        [JsonProperty("is_bundled_product")]
        public bool IsBundledProduct { get; set; }

        [JsonProperty("bin_picking_number")]
        public string BinPickingNumber { get; set; }

        [JsonProperty("primary_image")]
        public PrimaryImage Primary_image { get; set; }

        [JsonProperty("applied_discounts")]
        public IList<object> AppliedDiscounts { get; set; }

        [JsonProperty("product_options")]
        public IList<ProductOption> ProductOptions { get; set; }

        [JsonProperty("configurable_fields")]
        public List<ConfigurableFields> configurable_fields { get; set; }

        public string SalePrice { get; set; }

        public string UPC { get; set; }
    }

    public class ConfigurableFields
    {

        [JsonProperty("url")]
        public string url { get; set; }

        [JsonProperty("resource")]
        public string resource { get; set; }
    }

}