﻿namespace bigcommerce_jambu_returns.Models
{
    public class NoteUpdateRequest
    {
        public int returnId { get; set; }
        public string returnNotes { get; set; }
    }
}