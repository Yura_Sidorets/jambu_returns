﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace bigcommerce_jambu_returns.Models
{
   
    public class OlapicFeed
    {
        [XmlRoot(ElementName = "Category")]
        public class Category
        {
            [XmlElement(ElementName = "CategoryUniqueID")]
            public string CategoryUniqueID { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "CategoryParentID")]
            public string CategoryParentID { get; set; }
        }

        [XmlRoot(ElementName = "Categories")]
        public class Categories
        {
            [XmlElement(ElementName = "Category")]
            public List<Category> Category { get; set; }
        }

        [XmlRoot(ElementName = "CategoriesID")]
        public class CategoriesID
        {
            [XmlElement(ElementName = "CategoryID")]
            public List<string> CategoryID { get; set; }
        }

        [XmlRoot(ElementName = "UPCs")]
        public class UPCs
        {
            [XmlElement(ElementName = "UPC")]
            public List<string> UPC { get; set; }
        }

        [XmlRoot(ElementName = "Product")]
        public class Product
        {
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "ProductUniqueID")]
            public string ProductUniqueID { get; set; }
            [XmlElement(ElementName = "ProductUrl")]
            public string ProductUrl { get; set; }
            [XmlElement(ElementName = "ImageUrl")]
            public string ImageUrl { get; set; }
            [XmlElement(ElementName = "Description")]
            public string Description { get; set; }
            [XmlElement(ElementName = "Availability")]
            public string Availability { get; set; }
            [XmlElement(ElementName = "CategoriesID")]
            public CategoriesID CategoriesID { get; set; }
            [XmlElement(ElementName = "UPCs")]
            public UPCs UPCs { get; set; }
            [XmlElement(ElementName = "Color")]
            public string Color { get; set; }
            [XmlElement(ElementName = "ParentID")]
            public string ParentID { get; set; }
        }

        [XmlRoot(ElementName = "Products")]
        public class Products
        {
            [XmlElement(ElementName = "Product")]
            public List<Product> Product { get; set; }
        }

        [XmlRoot(ElementName = "Feed")]
        public class Feed
        {
            [XmlElement(ElementName = "Categories")]
            public Categories Categories { get; set; }
            [XmlElement(ElementName = "Products")]
            public Products Products { get; set; }

            public Feed()
            {
                Categories = new Categories();
                Products = new Products();
            }
        }
    }
}