﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bigcommerce_jambu_returns.Models
{
    public class OlapicProductPcModel
    {
        public OlapicFeed.Product Parent { get; set; }
        public List<OlapicFeed.Product> Children {get;set;}

        public OlapicProductPcModel()
        {
            Parent = new OlapicFeed.Product();
            Children = new List<OlapicFeed.Product>();
        }
    }
}