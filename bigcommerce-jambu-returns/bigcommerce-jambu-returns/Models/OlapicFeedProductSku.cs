﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{
    public class OlapicFeedProductSku 
    {
      

        public class SkuOption
        {

            [JsonProperty("product_option_id")]
            public int product_option_id { get; set; }

            [JsonProperty("option_value_id")]
            public int option_value_id { get; set; }
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("adjusted_price")]
        public string AdjustedPrice { get; set; }

        [JsonProperty("cost_price")]
        public string CostPrice { get; set; }

        [JsonProperty("upc")]
        public string Upc { get; set; }

        [JsonProperty("inventory_level")]
        public int InventoryLevel { get; set; }

        [JsonProperty("inventory_warning_level")]
        public int InventoryWarningLevel { get; set; }

        [JsonProperty("bin_picking_number")]
        public string BinPickingNumber { get; set; }

        [JsonProperty("weight")]
        public string Weight { get; set; }

        [JsonProperty("adjusted_weight")]
        public string AdjustedWeight { get; set; }

        [JsonProperty("is_purchasing_disabled")]
        public bool IsPurchasingDisabled { get; set; }

        [JsonProperty("purchasing_disabled_message")]
        public string PurchasingDisabledMessage { get; set; }

        [JsonProperty("image_file")]
        public string ImageFile { get; set; }

        [JsonProperty("options")]
        public IList<SkuOption> Options { get; set; }

        public string Color { get; set; }
    }
    

}