﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{
    public class FormData
    {

        public string customer { get; set; }

        public int order_id { get; set; }

        public string return_reason { get; set; }

        public string return_action { get; set; }

        public string return_comments { get; set; }

        public List<ReturnQty> return_qty { get; set; }
    }

    public class ReturnQty
    {

        public int? item_id { get; set; }

        public int quantity { get; set; }
    }
}