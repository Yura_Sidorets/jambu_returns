﻿namespace bigcommerce_jambu_returns.Models
{
    public class ReceiveTokenRequest
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Code { get; set; }
        public string Context { get; set; }
        public string GrantType { get { return "authorization_code"; } }
        public string RedirectUri { get; set; }
        public string Scope { get; set; }
    }
}