﻿using System.Collections.Generic;

namespace bigcommerce_jambu_returns.Models
{
    public class DeleteOrderRequest
    {
        public List<string> returns { get; set; }
    }
}