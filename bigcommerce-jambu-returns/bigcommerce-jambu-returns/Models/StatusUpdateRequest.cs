﻿namespace bigcommerce_jambu_returns.Models
{
    public class StatusUpdateRequest
    {
       public int returnId { get; set; } 
       public string returnStatus { get; set; }
    }
}