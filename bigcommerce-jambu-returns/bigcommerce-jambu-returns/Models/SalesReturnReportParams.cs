﻿using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{
    public class SalesReturnReportParams
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}