﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bigcommerce_jambu_returns.Models
{
    public class MailResponse
    {
        public byte success { get; set; }
    }
    public class MailResponseError
    {
        public string errors { get; set; }
    }
}