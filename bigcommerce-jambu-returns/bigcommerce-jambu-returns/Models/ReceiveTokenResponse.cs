﻿using bigcommerce_returns.Data;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{
    public class ReceiveTokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        public string Context { get; set; }
        public User User { get; set; }
        public string Scope { get; set; }
    }
}