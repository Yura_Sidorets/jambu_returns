﻿using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{
    public class CustomerAddresses
    {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("customer_id")]
            public int CustomerId { get; set; }

            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            [JsonProperty("last_name")]
            public string LastName { get; set; }

            [JsonProperty("company")]
            public string Company { get; set; }

            [JsonProperty("street_1")]
            public string Street1 { get; set; }

            [JsonProperty("street_2")]
            public string Street2 { get; set; }

            [JsonProperty("city")]
            public string City { get; set; }

            [JsonProperty("state")]
            public string State { get; set; }

            [JsonProperty("zip")]
            public string Zip { get; set; }

            [JsonProperty("country")]
            public string Country { get; set; }

            [JsonProperty("country_iso2")]
            public string CountryIso2 { get; set; }

            [JsonProperty("phone")]
            public string Phone { get; set; }
        }
    }