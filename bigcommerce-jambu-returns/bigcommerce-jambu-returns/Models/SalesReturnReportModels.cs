﻿using System.Collections.Generic;

namespace bigcommerce_jambu_returns.Models
{
    public class ReturnedObjectStats
    {
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Gender { get; set; }
        public double ReturnRate { get; set; }
        public double ReturnRateCost { get; set; }
        public double ShipQty { get; set; }
        public double ShipCost { get; set; }
        public double ReturnQty { get; set; }
        public double ReturnCost { get; set; }
        public double NetQty { get; set; }
        public double NetCost { get; set; }
        public Dictionary<Reason, int> Reason { get; set; }
    }

    public enum Reason
    {
        TooBig,
        TooSmall,
        TooWide,
        TooNarrow,
        DidNotLike,
        NotComfortable,
        ColorNotAsExpected,
        TwoSizes,
        Expectations,
        WrongShoe,
        TooLate,
        Defective,
        Other
    }

}