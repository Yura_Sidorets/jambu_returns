﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{
    public class OlapicFeedProductOption
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("option_id")]
        public int OptionId { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("sort_order")]
        public int SortOrder { get; set; }

        [JsonProperty("is_required")]
        public bool IsRequired { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("is_default")]
        public bool IsDefault { get; set; }


    }


}