﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{

    public class ShippingInfo
    {

        [JsonProperty("tracking_number")]
        public string tracking_number { get; set; }

        [JsonProperty("shipping_label")]
        public string shipping_label { get; set; }
    }

    public class Error
    {
        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }
    }

    public class OrderResponse
    {
        [JsonProperty("result")]
        public int result { get; set; }

        [JsonProperty("shipping_info")]
        public ShippingInfo shipping_info { get; set; }

        public OrderResponse()
        {
            shipping_info = new ShippingInfo();
        }
    }

    public class OrderResponseError
    {
        [JsonProperty("result")]
        public int result { get; set; }

        [JsonProperty("error")]
        public Error error { get; set; }
    }

    public class OrderResponseChecked
    {
        [JsonProperty("result")]
        public int result { get; set; }

        public List<ReturnProduct> return_product { get; set; }

    }

    public class ReturnProduct
    {
        public string name { get; set; }
        public int product_id { get; set; }
        public int max_qty { get; set; }
        public string price { get; set; }
        public string thumbnail_url { get; set; }
        public List<Option> options { get; set; }
    }

    public class Option
    {
        public string option { get; set; }
        public string value { get; set; }
    }

}