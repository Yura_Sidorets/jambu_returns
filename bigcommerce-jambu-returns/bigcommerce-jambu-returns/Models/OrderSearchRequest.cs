﻿namespace bigcommerce_jambu_returns.Models
{
    public class OrderSearchRequest
    {
        public string searchQuery { get; set; }
    }
}