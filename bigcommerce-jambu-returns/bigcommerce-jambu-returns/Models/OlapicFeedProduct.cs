﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Models
{
    public class PrimaryImage
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("zoom_url")]
        public string ZoomUrl { get; set; }

        [JsonProperty("thumbnail_url")]
        public string ThumbnailUrl { get; set; }

        [JsonProperty("standard_url")]
        public string StandardUrl { get; set; }

        [JsonProperty("tiny_url")]
        public string TinyUrl { get; set; }
    }

    public class Skus
    {

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("resource")]
        public string Resource { get; set; }
    }

    public class OlapicFeedProduct
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("categories")]
        public IList<int> Categories { get; set; }

        [JsonProperty("date_modified")]
        public string DateModified { get; set; }

        [JsonProperty("upc")]
        public string Upc { get; set; }

        [JsonProperty("custom_url")]
        public string CustomUrl { get; set; }

        [JsonProperty("primary_image")]
        public PrimaryImage PrimaryImage { get; set; }

        [JsonProperty("availability")]
        public string Availability { get; set; }

        [JsonProperty("skus")]
        public Skus Skus { get; set; }
    }


}