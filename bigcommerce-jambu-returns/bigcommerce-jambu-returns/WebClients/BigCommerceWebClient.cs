﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using bigcommerce_jambu_returns.Models;
using bigcommerce_returns.Data;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.WebClients
{
    public class BigCommerceWebClient : IBigCommerceWebClient
    {
        private string _clientId;
        public BigCommerceWebClient(string clientId)
        {
            _clientId = clientId;
        }

        public async Task<Order> GetOrderAsync(int orderId, string accessToken, string storeHash)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/orders/{orderId}");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return JsonConvert.DeserializeObject<Order>(json);
            }
        }

        public async Task<IEnumerable<Product>> GetOrderProductsAsync(int orderId, string accessToken, string storeHash)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/orders/{orderId}/products");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return JsonConvert.DeserializeObject<IEnumerable<Product>>(json);
            }
        }

        public async Task<IEnumerable<Product>> GetProductsAsync(string accessToken, string storeHash)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-Auth-Client", _clientId);
                client.DefaultRequestHeaders.Add("X-Auth-Token", accessToken);

                var result = await client.GetAsync($"https://api.bigcommerce.com/stores/{storeHash}/v2/products");

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);

                return JsonConvert.DeserializeObject<IEnumerable<Product>>(json);
            }
        }

        public async Task<ReceiveTokenResponse> ReceiveTokenAsync(ReceiveTokenRequest request)
        {
            using (var client = new HttpClient())
            {
                var parts = new[]
                {
                    $"client_id={request.ClientId}",
                    $"client_secret={request.ClientSecret}",
                    $"code={request.Code}",
                    $"scope={request.Scope}",
                    $"grant_type={request.GrantType}",
                    $"redirect_uri={request.RedirectUri}"
                };
                var queryString = string.Join("&", parts);
                var content = new StringContent(queryString, Encoding.UTF8, "application/x-www-form-urlencoded");
                var result = await client.PostAsync("https://login.bigcommerce.com/oauth2/token", content);

                var json = await result.Content.ReadAsStringAsync();

                if (result.StatusCode != HttpStatusCode.OK)
                    throw new InvalidOperationException(json);



                return JsonConvert.DeserializeObject<ReceiveTokenResponse>(json);
            }
        }
    }
}