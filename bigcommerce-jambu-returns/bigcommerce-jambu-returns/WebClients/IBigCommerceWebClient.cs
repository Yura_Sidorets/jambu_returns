﻿using System.Collections.Generic;
using System.Threading.Tasks;
using bigcommerce_jambu_returns.Models;
using bigcommerce_returns.Data;

namespace bigcommerce_jambu_returns.WebClients
{
    public interface IBigCommerceWebClient
    {
        Task<ReceiveTokenResponse> ReceiveTokenAsync(ReceiveTokenRequest request);
        Task<IEnumerable<Product>> GetProductsAsync(string accessToken, string storeHash);

        Task<IEnumerable<Product>> GetOrderProductsAsync(int orderId, string accessToken, string storeHash);
        Task<Order> GetOrderAsync(int orderId, string accessToken, string storeHash);
    }
}