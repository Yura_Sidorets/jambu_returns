﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages;
using bigcommerce_jambu_returns.Models;
using bigcommerce_jambu_returns.Properties;
using bigcommerce_returns.Data;
using bigcommerce_returns.Repositories;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Utils
{

    public static class ReportHelper
    {
        public static void GenerateReport(string connectionString)
        {
            ReturnRepository repository = new ReturnRepository(connectionString);
            var merchant = new MerchantRepository(connectionString);
            OrdersHelper ordersHelper = new OrdersHelper(merchant.GetMerchantByClientKey(Settings.Default.ClientId).StoreHash, merchant.GetMerchantByClientKey(Settings.Default.ClientId).AccessToken);
            var returns = repository.GetAllApproved();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            UTF8Encoding utf8Encoding = new UTF8Encoding();
            using (MemoryStream ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms, utf8Encoding);

                if (returns != null)
                {
                    sw.WriteLine("sep=,");
                    sw.WriteLine("ReturnNum, OrderDate, OrderTime," +
                                 " OriginalOrderNum, RestockingFee, " +
                                 "ShippingCost,CustomerNotes,FirstName," +
                                 "LastName,AddressLine1,AddressLine2," +
                                 "City,State,Country," +
                                 "PostalCode,Phone,LineNum," +
                                 "UPC,OrderQty,SalesPrice," +
                                 "Tax,LPN,ReturnReason," +
                                 "ReturnInspection");

                    foreach (var returnOrder in returns)
                    {
                        try
                        {
                            //Get Tax and Shipping cost from order
                            var order = ordersHelper.GetOrder(returnOrder.OrderId.ToString());
                            returnOrder.ShippingCost = "-5.00";
                            returnOrder.Tax = "0.00"; //order.TotalTax;
                            DateTime orderDateTime = DateTime.Parse(order.DateCreated);
                            //Get Sales Price from Product
                            var prod = ordersHelper.GetProductSalePrice(returnOrder.ProductId);
                            //Get UPC friom product SKU
                            var prodSku = JsonConvert.DeserializeObject<List<OlapicFeedProductSku>>(RequestHelper.GetResponse(new Uri(prod.Skus.url), merchant.GetMerchantByClientKey(Settings.Default.ClientId).AccessToken));
                            returnOrder.UPC = " ";
                            var sku = prodSku.Find(productSku => productSku.Sku.Equals(returnOrder.SKU));
                            returnOrder.UPC = sku.Upc;
                            if (prod.SalePrice == null || prod.Price.IsEmpty() ||
                                (Convert.ToDecimal(prod.SalePrice) == 0))
                            {
                                if (Convert.ToDecimal(sku.Price) == 0)
                                {
                                    returnOrder.SalesPrice = sku.AdjustedPrice.Remove(sku.AdjustedPrice.Length - 2, 2);
                                }
                                else
                                {
                                    returnOrder.SalesPrice = sku.Price.Remove(sku.Price.Length - 2, 2); ;
                                }
                            }
                            else
                            {
                                returnOrder.SalesPrice = prod.SalePrice;
                            }
                            //Get order shipping 
                            var street1 = " ";
                            var street2 = " ";
                            Shipping shipping = new Shipping();
                            try
                            {
                                var orderShipping = ordersHelper.GetShippings(order.Id.ToString());
                                if (orderShipping == null || orderShipping.Count == 0)
                                    continue;


                                street1 = orderShipping[0].ShippingAddress.Street1;
                                if (orderShipping[0].ShippingAddress.Street1.Contains(","))
                                {
                                    street1 = orderShipping[0].ShippingAddress.Street1.Trim(',').Replace(",", "");
                                }
                                street2 = orderShipping[0].ShippingAddress.Street2;
                                if (orderShipping[0].ShippingAddress.Street2.Contains(","))
                                {
                                    street2 = orderShipping[0].ShippingAddress.Street2.Trim(',').Replace(",", "");
                                }
                                shipping = orderShipping[0];
                            }
                            catch
                            {
                            }


                            sw.WriteLine($"{returnOrder.ReturnId},{orderDateTime.Date.ToShortDateString()},{orderDateTime.TimeOfDay}," +
                                         $"{order.Id},0,{returnOrder.ShippingCost}," +
                                         $"{returnOrder.Notes},{shipping.BillingAddress.FirstName},{shipping.BillingAddress.LastName}," +
                                         $"{street1},{street2},{shipping.ShippingAddress.City}," +
                                         $"{shipping.ShippingAddress.State},{shipping.ShippingAddress.Country},{shipping.ShippingAddress.Zip}," +
                                         $"{shipping.ShippingAddress.Phone},1,{returnOrder.UPC}," +
                                         $"{returnOrder.Quantity},{returnOrder.SalesPrice},{returnOrder.Tax}," +
                                         $"0,{returnOrder.Reason}, ");
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }
                else
                {
                    sw.WriteLine("Message");
                    sw.WriteLine("No returns today");
                }

                sw.Flush();
                ms.Position = 0;

                //Create a FTP Request Object and Specfiy a Complete Path
                FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create("ftp://66.234.228.199:21/Returns/" + "Returns_" + DateTime.Now.Year + DateTime.Now.Day + DateTime.Now.Month + "_" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".csv");
                reqObj.Credentials = new NetworkCredential("ecommerce", "ecommerce!%");
                //Call A FileUpload Method of FTP Request Object
                reqObj.Method = WebRequestMethods.Ftp.UploadFile;

                reqObj.EnableSsl = false;
                reqObj.UsePassive = true;
                reqObj.UseBinary = true;
                reqObj.KeepAlive = true;
                reqObj.Proxy = null;

                //If you want to access Resourse Protected,give UserName and PWD

                // Copy the contents of the file to the byte array.
                byte[] fileContents = ms.ToArray();
                reqObj.ContentLength = fileContents.Length;

                //Upload File to FTPServer
                Stream requestStream = reqObj.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();
                FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
                response.Close();

                //Mailing with return file if needed
                //MailMessage mail = new MailMessage();
                //mail.To.Add("sidoretsyura@gmail.com");
                //mail.From = new MailAddress("returnsapp@vidagroup.com");
                //mail.Subject = "Returns Daily Report";
                //string Body = "";
                //mail.Body = Body;
                //mail.IsBodyHtml = true;
                //mail.Attachments.Add(new Attachment(ms, "Returns_" + DateTime.Now.Year + DateTime.Now.Day + DateTime.Now.Month + "_" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".csv"));
                //using (var smtp = new SmtpClient())
                //{
                //    var credential = new NetworkCredential
                //    {
                //        // replace with the vidagroup smtp credentials
                //        UserName = "sidoretsyura@gmail.com",
                //        Password = "B5nwAzX3FIaT"
                //    };
                //    smtp.Credentials = credential;
                //    smtp.Host = "mail.smtp2go.com"; //replace with vidagroup smtp if needed
                //    smtp.Port = 2525;
                //    smtp.EnableSsl = true;
                //    smtp.Send(mail);
                //}

            }
        }

        public static void GenerateSalesReport(DateTime start, DateTime end, string connectionString)
        {
            var repository = new ReturnRepository(connectionString);
            var merchant = new MerchantRepository(connectionString);
            var ordersHelper = new OrdersHelper(merchant.GetMerchantByClientKey(Settings.Default.ClientId).StoreHash, merchant.GetMerchantByClientKey(Settings.Default.ClientId).AccessToken);

            var shipQty = new List<ReturnRequestProduct>();
            CalculateShippedQty(start, end, ref shipQty, ordersHelper);

            var bigReturnsSet = repository.GetAllReturnsByDate(start.ToShortDateString(), end.ToShortDateString());
            var smallReturnSet = bigReturnsSet.Where(returnOrder => DateTime.Parse(returnOrder.Date) >= start).ToList();
            if (smallReturnSet.Count == 0) return;

            var returnedObjectStat = new Dictionary<string, ReturnedObjectStats>();
            var returnedObjectStat1 = new Dictionary<string, ReturnedObjectStats>();

            var completeReportStat = new ReturnedObjectStats();
            var shipPart = shipQty.Select((x, i) => new { x, i }).Where(t => t.i % 2 == 0).Select(t => t.x);
            var shipPart2 = shipQty.Select((x, i) => new { x, i }).Where(t => t.i % 2 != 0).Select(t => t.x);
            Parallel.Invoke(
            () =>
            {
                foreach (var shipping in shipPart)
                {
                    var product = new Return
                    {
                        Name = shipping.Name,
                        SalesPrice = shipping.SalePrice,
                        SKU = shipping.Sku,
                        Quantity = shipping.Quantity,
                        ProductId = shipping.ProductId,
                        OrderId = shipping.OrderId,
                        ProductName = shipping.Name
                    };

                    ProcessOrder(product, ordersHelper, merchant, ref returnedObjectStat1, true);
                }
            }, 
            () =>
            {
                foreach (var shipping in shipPart2)
                {
                    var product = new Return
                    {
                        Name = shipping.Name,
                        SalesPrice = shipping.SalePrice,
                        SKU = shipping.Sku,
                        Quantity = shipping.Quantity,
                        ProductId = shipping.ProductId,
                        OrderId = shipping.OrderId,
                        ProductName = shipping.Name
                    };

                    ProcessOrder(product, ordersHelper, merchant, ref returnedObjectStat, true);
                }
            });
            foreach (var obj in returnedObjectStat1)
            {
                if (!returnedObjectStat.ContainsKey(obj.Key))
                {
                    returnedObjectStat.Add(obj.Key, obj.Value);
                }
                else
                {
                    returnedObjectStat[obj.Key].ShipQty += obj.Value.ShipQty;
                    returnedObjectStat[obj.Key].ReturnQty += obj.Value.ReturnQty;

                    returnedObjectStat[obj.Key].NetQty = returnedObjectStat[obj.Key].ShipQty - returnedObjectStat[obj.Key].ReturnQty;
                    returnedObjectStat[obj.Key].NetCost = returnedObjectStat[obj.Key].ShipCost - returnedObjectStat[obj.Key].ReturnCost;
                }
            }

            foreach (var returnOrder in smallReturnSet)
            {
                ProcessOrder(returnOrder, ordersHelper, merchant, ref returnedObjectStat, false);
            }
            ProcessCompleteProductsStat(ref completeReportStat, ref returnedObjectStat);

            ExcelHelper.CreateExcel(ref returnedObjectStat, ref completeReportStat, start.ToShortDateString(), end.ToShortDateString());
        }

        private static void GetShipments(DateTime start, DateTime end, ref List<ReturnRequestOrder> shipments, OrdersHelper ordersHelper)
        {
            List<ReturnRequestOrder> shipmentsTmp;
            var i = 1;
            do
            {
                shipmentsTmp = ordersHelper.GetOrdersByDate(start, end, i);
                if (shipmentsTmp != null)
                {
                    var breakPoint = false;
                    foreach (var shpmnt in shipmentsTmp)
                    {
                        if (shpmnt.DateShipped.Equals("")) continue;
                        var date = DateTime.Parse(shpmnt.DateShipped);
                        if (date < start)
                        {
                            breakPoint = true;
                            break;
                        }
                        if (date > start && date < end)
                        {
                            shipments.Add(shpmnt);
                        }
                    }
                    if (breakPoint)
                    {
                        break;
                    }
                    i++;
                }
                else
                    break;
            } while (shipmentsTmp.Count > 0);
        }

        private static void CalculateShippedQty(DateTime start, DateTime end, ref List<ReturnRequestProduct> shipList, OrdersHelper ordersHelper)
        {
            var shipments = new List<ReturnRequestOrder>();
            GetShipments(start, end, ref shipments, ordersHelper);

            foreach (var shipment in shipments)
            {
                try
                {
                    shipList.AddRange(ordersHelper.GetProducts(shipment.Id.ToString()));
                }
                catch (Exception e)
                {
                }
            }
        }

        private static string PrepareSku(string initSku)
        {
            var sku = initSku;
            try
            {
                sku = OlapicHelper.PrepareSku(sku);
            }
            catch
            {
            }
            return sku.IndexOf("-", StringComparison.Ordinal) != -1 ? sku.Remove(sku.IndexOf("-", StringComparison.Ordinal) - 2, 4) : sku.Remove(sku.Length - 5, 4);
        }

        private static void ProcessReturnProduct(ref ReturnedObjectStats returnedObject, Return returnOrder, double refundedAmount, bool shipped)
        {
            if (!shipped)
            {
                if (returnedObject.ShipQty == 0)
                {
                    return;
                }

                returnedObject.ReturnQty += 1;
                returnedObject.ReturnCost = returnedObject.ReturnQty * refundedAmount;
            }
            else
            {
                returnedObject.ShipQty += 1;
            }
            //if (Math.Abs(returnedObject.ShipQty) <= 0)
            //{
            //    returnedObject.ShipQty += 1;
            //}
            if (Math.Abs(returnedObject.ShipQty) > 0.00)
            {
                returnedObject.ShipCost = Math.Round(returnedObject.ShipQty * double.Parse(returnOrder.SalesPrice),2);
            }

            returnedObject.ReturnRate = Math.Round(returnedObject.ReturnQty / returnedObject.ShipQty * 100, 2);
            returnedObject.ReturnRateCost = Math.Round(returnedObject.ReturnCost / returnedObject.ShipCost * 100, 2);

            if (shipped)
            {
                returnedObject.ReturnRateCost = 0;
            }

            returnedObject.NetQty = Math.Round(returnedObject.ShipQty - returnedObject.ReturnQty,2);
            returnedObject.NetCost = Math.Round(returnedObject.ShipCost - returnedObject.ReturnCost,2);
        }

        private static void ProcessCompleteProductsStat(ref ReturnedObjectStats completeReportStat, ref Dictionary<string, ReturnedObjectStats> returnedObjectStat)
        {
            completeReportStat.Reason = new Dictionary<Reason, int>
            {
                {Reason.ColorNotAsExpected, 0},
                {Reason.Defective, 0},
                {Reason.TooNarrow, 0},
                {Reason.DidNotLike, 0},
                {Reason.Expectations, 0},
                {Reason.Other, 0},
                {Reason.TooLate, 0},
                {Reason.TwoSizes, 0},
                {Reason.WrongShoe, 0},
                {Reason.NotComfortable, 0},
                {Reason.TooBig, 0},
                {Reason.TooSmall, 0},
                {Reason.TooWide, 0}
            };

            foreach (var stat in returnedObjectStat.Values)
            {

                completeReportStat.ShipQty += stat.ShipQty;
                completeReportStat.ShipCost += Math.Round(stat.ShipCost,2);

                completeReportStat.ReturnQty += stat.ReturnQty;
                completeReportStat.ReturnCost += Math.Round(stat.ReturnCost,2);

                completeReportStat.ReturnRate = Math.Round(completeReportStat.ReturnQty / completeReportStat.ShipQty * 100, 2);

                completeReportStat.ReturnRateCost = Math.Round(completeReportStat.ReturnCost / completeReportStat.ShipCost * 100, 2);

                completeReportStat.NetQty += stat.NetQty;
                completeReportStat.NetCost += Math.Round(stat.NetCost,2);

                foreach (var key in stat.Reason)
                {
                    if (completeReportStat.Reason.ContainsKey(key.Key))
                    {
                        completeReportStat.Reason[key.Key] += stat.Reason[key.Key];
                    }
                    else
                    {
                        completeReportStat.Reason.Add(key.Key, stat.Reason[key.Key]);
                    }
                }
            }
        }

        private static Tuple<Reason, int> ProcessReturnReason(ReturnedObjectStats returnedObject, string reason)
        {
            switch (reason)
            {
                case "Too big":
                    {
                        return new Tuple<Reason, int>(Reason.TooBig, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Too small":
                    {
                        return new Tuple<Reason, int>(Reason.TooSmall, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Too wide":
                    {
                        return new Tuple<Reason, int>(Reason.TooWide, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Too narrow":
                    {
                        return new Tuple<Reason, int>(Reason.TooNarrow, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Did not like":
                    {
                        return new Tuple<Reason, int>(Reason.DidNotLike, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Not comfortable":
                    {
                        return new Tuple<Reason, int>(Reason.NotComfortable, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Color not as expected":
                    {
                        return new Tuple<Reason, int>(Reason.ColorNotAsExpected, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Ordered 2 sizes and returning one":
                    {
                        return new Tuple<Reason, int>(Reason.TwoSizes, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Did not meet value expectations":
                    {
                        return new Tuple<Reason, int>(Reason.Expectations, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Wrong shoe shipped":
                    {
                        return new Tuple<Reason, int>(Reason.WrongShoe, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Received too late":
                    {
                        return new Tuple<Reason, int>(Reason.TooLate, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Defective":
                    {
                        return new Tuple<Reason, int>(Reason.Defective, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                case "Other":
                    {
                        return new Tuple<Reason, int>(Reason.Other, Convert.ToInt32(returnedObject.ReturnQty));
                    }
                default:
                    {
                        return new Tuple<Reason, int>(Reason.Other, Convert.ToInt32(returnedObject.ReturnQty));
                    }
            }
        }

        private static void ProcessOrder(Return returnOrder, OrdersHelper ordersHelper, MerchantRepository merchant, ref Dictionary<string, ReturnedObjectStats> returnedObjectStat, bool shipped)
        {
            try
            {
                var sku = PrepareSku(returnOrder.SKU);

                var initOrder = ordersHelper.GetOrder(returnOrder.OrderId.ToString());
                var prod = ordersHelper.GetProductSalePrice(returnOrder.ProductId);

                var prodSku = JsonConvert.DeserializeObject<List<OlapicFeedProductSku>>(RequestHelper.GetResponse(new Uri(prod.Skus.url), merchant.GetMerchantByClientKey(Settings.Default.ClientId).AccessToken));
                var skuObject = prodSku.Find(productSku => productSku.Sku.Equals(returnOrder.SKU));
                var refundedAmount = double.Parse(initOrder.RefundedAmount) / initOrder.ItemsTotal;
                if (returnOrder.SalesPrice == null)
                    returnOrder.SalesPrice = "0.00";
                try
                {
                    if (prod.SalePrice == null || prod.SalePrice.Equals("0.0000"))
                    {
                        returnOrder.SalesPrice = skuObject?.Price ?? skuObject.AdjustedPrice;
                        if (returnOrder.SalesPrice.Equals("0.00"))
                        {
                            return;
                            returnOrder.SalesPrice = refundedAmount.ToString(CultureInfo.InvariantCulture);
                        }
                    }
                    else
                    {
                        returnOrder.SalesPrice = prod.SalePrice;
                    }
                    if (returnOrder.SalesPrice.Equals("0.00"))
                    {
                        return;
                    }
                }
                catch (Exception e)
                {

                }

                if (!returnedObjectStat.ContainsKey(prod.Name))
                {
                    var returnedObject = new ReturnedObjectStats
                    {
                        ItemNo = sku,
                        Description = returnOrder.ProductName
                    };

                    ProcessReturnProduct(ref returnedObject, returnOrder, refundedAmount, shipped);

                    returnedObject.Reason = new Dictionary<Reason, int>
                        {
                            {Reason.ColorNotAsExpected, 0},
                            {Reason.Defective, 0},
                            {Reason.TooNarrow, 0},
                            {Reason.DidNotLike, 0},
                            {Reason.Expectations, 0},
                            {Reason.Other, 0},
                            {Reason.TooLate, 0},
                            {Reason.TwoSizes, 0},
                            {Reason.WrongShoe, 0},
                            {Reason.NotComfortable, 0},
                            {Reason.TooBig, 0},
                            {Reason.TooSmall, 0},
                            {Reason.TooWide, 0}
                        };
                    if (!shipped)
                    {
                        if (returnedObject.ShipQty == 0)
                        {
                            return;
                        }
                        var returnReason = ProcessReturnReason(returnedObject, returnOrder.Reason);
                        if (!returnedObject.Reason.ContainsKey(returnReason.Item1))
                        {
                            returnedObject.Reason.Add(returnReason.Item1, 1);
                        }
                        else
                        {
                            returnedObject.Reason[returnReason.Item1] += 1;
                        }

                    }

                    returnedObjectStat.Add(prod.Name, returnedObject);
                }
                else
                {
                    var returnedObject = returnedObjectStat[prod.Name];
                    ProcessReturnProduct(ref returnedObject, returnOrder, refundedAmount, shipped);

                    if (!shipped)
                    {
                        if (returnedObject.ShipQty == 0)
                        {
                            return;
                        }
                        var returnReason = ProcessReturnReason(returnedObject, returnOrder.Reason);
                        if (!returnedObject.Reason.ContainsKey(returnReason.Item1))
                        {
                            returnedObject.Reason.Add(returnReason.Item1, 1);
                        }
                        else
                        {
                            returnedObject.Reason[returnReason.Item1] += 1;
                        }
                    }
                   
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}