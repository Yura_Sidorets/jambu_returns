﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace bigcommerce_jambu_returns.Utils
{
    public static class FileHelper
    {
        public static int SaveShippingLabel(string label, string orderId)
        {

            var filename = "Shipping-label-order-" + orderId;
            var hash = filename.GetHashCode();
            string path = HttpContext.Current.Server.MapPath("~/Labels/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            Stream writer = new FileStream(Path.Combine(path, hash + ".gif"), FileMode.Create);
            var file = Convert.FromBase64String(label);
            writer.Write(file,0, file.Length);
            writer.Close();
            return hash;
        }
    }
}