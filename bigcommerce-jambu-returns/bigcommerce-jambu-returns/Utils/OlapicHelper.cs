﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.WebPages;
using System.Xml.Serialization;
using bigcommerce_jambu_returns.Models;
using bigcommerce_jambu_returns.Properties;
using bigcommerce_returns.Data;
using bigcommerce_returns.Repositories;
using Microsoft.Ajax.Utilities;

namespace bigcommerce_jambu_returns.Utils
{

    public static class OlapicHelper
    {
        public static void PostFeed(string connectionString)
        {
            var feed = GenerateFeed(connectionString);

            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(OlapicFeed.Feed));

                serializer.Serialize(ms, feed);
                ms.Position = 0;
                ms.Close();
                //Create a FTP Request Object and Specfiy a Complete Path
                FtpWebRequest reqObj =
                    (FtpWebRequest)
                        WebRequest.Create(@"ftp://54.243.138.44:21/" + "OlapicJAMBUProductFeed_" + DateTime.Now.Year +
                                          DateTime.Now.Day + DateTime.Now.Month + "_" + DateTime.Now.Hour +
                                          DateTime.Now.Minute + DateTime.Now.Second + "_" + DateTime.Now.Millisecond +
                                          DateTime.Now.Ticks + ".xml");

                //Call A FileUpload Method of FTP Request Object
                reqObj.Method = WebRequestMethods.Ftp.UploadFile;

                reqObj.UseBinary = true;
                reqObj.KeepAlive = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                       SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                //If you want to access Resourse Protected,give UserName and PWD
                reqObj.Credentials = new NetworkCredential(@"jambu", @"Hohc1dooAegh0air");
                // Copy the contents of the file to the byte array.
                byte[] fileContents = ms.ToArray();
                reqObj.ContentLength = fileContents.Length;

                //Upload File to FTPServer
                Stream requestStream = reqObj.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();
                FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
                response.Close();
            }
        }

        public static OlapicFeed.Feed GenerateFeed(string connectionString)
        {
            var merchant = new MerchantRepository(connectionString);
            var signedPayload = merchant.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload;
            var store = StoreProvider.ReadStore(signedPayload, Settings.Default.ClientSecret);

            OrdersHelper ordersHelper = new OrdersHelper(merchant.Get(store.Owner.Id).StoreHash,
                merchant.Get(store.Owner.Id).AccessToken);

            #region Get Categories

            var categories = new List<Category>();
            var categoriesTmp = new List<Category>();
            int i = 1;
            do
            {
                categoriesTmp = ordersHelper.GetCategories(i);
                if (categoriesTmp != null)
                {
                    categories.AddRange(categoriesTmp);
                    i++;
                }
                else
                    break;
            } while (categoriesTmp.Count > 0);

            var feed = new OlapicFeed.Feed();
            feed.Categories = new OlapicFeed.Categories { Category = new List<OlapicFeed.Category>() };

            foreach (var category in categories)
            {
                feed.Categories.Category.Add(new OlapicFeed.Category
                {
                    CategoryUniqueID = category.Id.ToString(),
                    Name = category.Name,
                    CategoryParentID = category.ParentId.ToString()
                });
            }

            #endregion

            #region Get Products

            var products = new List<OlapicFeedProduct>();
            var productsTmp = new List<OlapicFeedProduct>();
            i = 1;
            do
            {
                productsTmp = ordersHelper.GetProductsForOlapic(i);
                if (productsTmp != null)
                {
                    products.AddRange(productsTmp);
                    i++;
                }
                else
                    break;
            } while (productsTmp.Count > 0);

            var skus = new List<OlapicFeedProductSku>();
            foreach (var product in products)
            {
                var skusRange = ordersHelper.GetOlapicFeedProductSkus(product.Skus.Url);
                if (skusRange != null)
                    skus.AddRange(skusRange);
                //var prod = ordersHelper.GetProductsInfoForOlapic(product.Id);
                //if (prod?.Parent != null || prod?.Children != null)
                //{
                //    feed.Products.Product.Add(prod.Parent);
                //    feed.Products.Product.AddRange(prod.Children);
                //}                
            }

            var uniqueSkus = new List<OlapicFeedProductSku>();
            foreach (var sku in skus)
            {
                if (sku.Upc.IsEmpty() || sku.Upc.IsNullOrWhiteSpace())
                {
                    continue;
                }
                sku.Sku = PrepareSku(sku.Sku);
            }

            foreach (var sku in skus)
            {
                uniqueSkus.Sort(new SkuComparer());
                var prodSkuPos = uniqueSkus.BinarySearch(sku, new SkuComparer());

                if (prodSkuPos >= 0)
                {
                    var upc = uniqueSkus[prodSkuPos].Upc;
                    if (!upc.Contains(sku.Upc))
                    {
                        uniqueSkus[prodSkuPos].Upc += "/" + sku.Upc;
                    }
                }
                else
                {
                    var color = "";
                    ordersHelper.GetOlapicFeedProductSkuColor(sku, ref color);
                    sku.Color = color;
                    uniqueSkus.Add(sku);
                }
            }

            feed.Products = new OlapicFeed.Products { Product = new List<OlapicFeed.Product>() };
            products.Sort(new ProductComparer());

            var temp = new OlapicFeed.Products { Product = new List<OlapicFeed.Product>() };
            foreach (var product in products)
            {
                var last = CreateProduct(product);
                temp.Product.Add(last);
                foreach (var sku in uniqueSkus)
                {
                    if (product.Id.Equals(sku.ProductId))
                    {
                        temp.Product.Add(CreateProductFromSku(sku, ref last));
                    }
                }
            }

            foreach (var product in temp.Product)
            {
                if (product.UPCs.UPC.Count != 0)
                {
                    feed.Products.Product.Add(product);
                }
            }
            #endregion
            return feed;
        }

        public static string PrepareSku(string sku)
        {
            string compSku = sku.Remove(sku.LastIndexOf("-", StringComparison.Ordinal));

            var split = sku.Split('-');
            if (split.Length >= 3)
            {
                compSku = split[0] + "-" + split[1];
            }
            return compSku;
        }

        private static OlapicFeed.Product CreateProductFromSku(OlapicFeedProductSku sku, ref OlapicFeed.Product parent)
        {
            if (sku.ImageFile.IsNullOrWhiteSpace() || sku.ImageFile.IsEmpty())
            {
                sku.ImageFile = parent.ImageUrl;
            }
            var prodsku = new OlapicFeed.Product()
            {
                Availability = parent.Availability,
                CategoriesID =
                    new OlapicFeed.CategoriesID() { CategoryID = parent.CategoriesID.CategoryID },
                Color = sku.Color,
                Description = parent.Description,
                ImageUrl = sku.ImageFile,
                Name = parent.Name + " " + sku.Color,
                ParentID = parent.ProductUniqueID,
                ProductUniqueID =
                    sku.Sku,
                ProductUrl = parent.ProductUrl,
                UPCs = new OlapicFeed.UPCs() { UPC = new List<string>() }
            };
            if (!sku.Upc.IsNullOrWhiteSpace() && !sku.Upc.IsEmpty())
            {
                var split = sku.Upc.Split('/');
                prodsku.UPCs.UPC.AddRange(split);
                parent.UPCs.UPC.AddRange(split);
                parent.UPCs.UPC = parent.UPCs.UPC.Distinct().ToList();
                if (parent.UPCs.UPC.Count == 0)
                {
                    parent = null;
                }
            }
            return prodsku;
        }

        private static OlapicFeed.Product CreateProduct(OlapicFeedProduct product)
        {
            var outProduct = new OlapicFeed.Product();

            if (product.Availability.Equals("available"))
            {
                outProduct.Availability = "1";
            }
            else { outProduct.Availability = "0"; }
            outProduct.ProductUniqueID = product.Sku.Trim(' ');

            outProduct.CategoriesID = new OlapicFeed.CategoriesID
            {
                CategoryID = new List<string>()
            };
            foreach (var category in product.Categories)
            {
                outProduct.CategoriesID.CategoryID.Add(category.ToString());
            }
            outProduct.Name = product.Name;
            if (product.PrimaryImage.StandardUrl.IsNullOrWhiteSpace() || product.PrimaryImage.StandardUrl.IsEmpty())
            {
                product.PrimaryImage.StandardUrl = "http://nrgene.com/wp-content/plugins/lightbox/images/No-image-found.jpg";
            }
            outProduct.ImageUrl = product.PrimaryImage.StandardUrl;
            outProduct.Description = StripHTML(product.Description); //TODO: Clear from HTML
            var strings =
                outProduct.Description.Replace("\"", "")
                    .Replace(">", "")
                    .Replace("<", "")
                    .Replace("&nbsp;", " ")
                    .Replace("Key Features", "")
                    .Replace("\r\n", "")
                    .Trim(' ')
                    .Split('.');
            strings = strings.Distinct().ToArray();
            outProduct.Description = "";
            foreach (var line in strings)
            {
                outProduct.Description += line + ". ";
            }
            outProduct.ProductUrl = "http://www.jambu.com" + product.CustomUrl;
            outProduct.UPCs = new OlapicFeed.UPCs();
            outProduct.UPCs.UPC = new List<string>();
            return outProduct;
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", " ");
        }
    }


    class PartialComparer : IEqualityComparer<OlapicFeedProductSku>
    {
        public bool Equals(OlapicFeedProductSku x, OlapicFeedProductSku y)
        {
            return x.Sku.Equals(y.Sku);
        }

        public int GetHashCode(OlapicFeedProductSku obj)
        {
            return obj.GetHashCode();
        }
    }

    class SkuComparer : IComparer<OlapicFeedProductSku>
    {
        public int Compare(OlapicFeedProductSku x, OlapicFeedProductSku y)
        {

            if (x == y) return 0;
            if (x == null) return -1;
            if (y == null) return 1;

            return String.Compare(x.Sku, y.Sku);
        }
    }

    class ProductComparer : IComparer<OlapicFeedProduct>
    {
        public int Compare(OlapicFeedProduct x, OlapicFeedProduct y)
        {

            if (x == y) return 0;
            if (x == null) return -1;
            if (y == null) return 1;

            return String.Compare(x.Id.ToString(), y.Id.ToString());
        }
    }
}