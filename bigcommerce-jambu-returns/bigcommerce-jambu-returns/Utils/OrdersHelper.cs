﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.WebPages;
using bigcommerce_jambu_returns.Models;
using bigcommerce_returns.Data;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.Utils
{
    public class OrdersHelper
    {
        private string Token;
        private readonly string _baseUrl;

        public OrdersHelper(string storeHash, string token)
        {
            Token = token;
            _baseUrl = $"https://api.bigcommerce.com/stores/{storeHash}/v2";
        }

        public StoreInfo GetStoreInfo()
        {
            var response = RequestHelper.GetResponse(new Uri(_baseUrl + "/store"), Token);
            return JsonConvert.DeserializeObject<StoreInfo>(response);
        }

        public List<Customer> GetCustomers(string email)
        {
            // email = email.Replace("+", "*");
            var eamil = HttpUtility.UrlEncode(HttpUtility.UrlEncode(email));
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/customers?email=" + email), Token);
            return JsonConvert.DeserializeObject<List<Customer>>(jsonResponse);
        }

        public Customer GetCustomerById(string id)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/customers/" + id), Token);
            return JsonConvert.DeserializeObject<Customer>(jsonResponse);
        }

        public ReturnRequestOrder GetOrder(string id)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/orders/" + id), Token);
            return JsonConvert.DeserializeObject<ReturnRequestOrder>(jsonResponse);
        }

        public List<ReturnRequestOrder> GetOrdersByDate(DateTime startDate, DateTime endDate,int page)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/orders" + $"?page={page}&limit=250&sort=date_created:desc"), Token);
            return JsonConvert.DeserializeObject<List<ReturnRequestOrder>>(jsonResponse);
        }

        public List<Shipping> GetShippings(string orderId)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/orders/" + orderId + "/shipments"), Token);
            return JsonConvert.DeserializeObject<List<Shipping>>(jsonResponse);
        }

        public List<ReturnRequestProduct> GetProducts(string orderId)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/orders/" + orderId + "/products"), Token);
            return JsonConvert.DeserializeObject<List<ReturnRequestProduct>>(jsonResponse);
        }

        public Product GetProduct(int productId)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/products/" + productId+ "?include="), Token);
            return JsonConvert.DeserializeObject<Product>(jsonResponse);
        }

        public Product GetProductInfo(int productId)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/products/" + productId +
                                     "?include=custom_url,price,weight,sku"), Token);
            return JsonConvert.DeserializeObject<Product>(jsonResponse);
        }

        public Product GetProductSalePrice(int productId)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/products/" + productId +
                         "?include=sale_price,skus"), Token);
            return JsonConvert.DeserializeObject<Product>(jsonResponse);
        }

        public Product FilterProductBySku(string sku)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/products/" +
                                     "?sku=" + sku), Token);
            return JsonConvert.DeserializeObject<Product>(jsonResponse);
        }

        public List<ProductImage> ListProductImages(int productId)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/products/" + productId +"/images"), Token);
            return JsonConvert.DeserializeObject<List<ProductImage>>(jsonResponse);
        }

        public List<Category> GetCategories(int page)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + $"/categories?page={page}&limit=250"), Token);
            return JsonConvert.DeserializeObject<List<Category>>(jsonResponse);
        }

        public ProductSku GetProductSku(int productId, string sku)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + $"/products/{productId}/skus/{sku}"), Token);
            return JsonConvert.DeserializeObject<ProductSku>(jsonResponse);
        }

        public CustomerAddresses GetCustomerAddresses(string uri)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(uri), Token);
            return JsonConvert.DeserializeObject<CustomerAddresses>(jsonResponse);
        }


        //Olapic Feature methods
        public List<OlapicFeedProduct> GetProductsForOlapic(int page)
        {
            var jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/products" + $"?page={page}&limit=250"), Token);

            return JsonConvert.DeserializeObject<List<OlapicFeedProduct>>(jsonResponse);
        }

        public void GetProductAdditionalInfoForOlapic(ref OlapicFeedProduct product)
        {
            string jsonResponse = RequestHelper.GetResponse(new Uri(_baseUrl + "/products/" + product.Id + "?include=upc,skus"), Token);
            var prod = JsonConvert.DeserializeObject<OlapicFeedProduct>(jsonResponse);
            product.Upc = prod.Upc;
            product.Skus = prod.Skus;
        }

        public List<OlapicFeedProductSku> GetOlapicFeedProductSkus(string skus)
        {
            return JsonConvert.DeserializeObject<List<OlapicFeedProductSku>>(RequestHelper.GetResponse(new Uri(skus), Token));
        }

        public void GetOlapicFeedProductSkuColor(OlapicFeedProductSku sku, ref string color)
        {
            foreach (var option in sku.Options)
            {
                try
                {
                    OlapicFeedProductOption prodOption = JsonConvert.DeserializeObject<OlapicFeedProductOption>(
                        RequestHelper.GetResponse(
                            new Uri(_baseUrl + $"/products/{sku.ProductId}/options/{option.product_option_id}"),
                            Token));
                    if (prodOption.DisplayName.Equals("Color"))
                    {
                        List<OlapicFeedProductOption> options = JsonConvert.DeserializeObject<List<OlapicFeedProductOption>>(
                            RequestHelper.GetResponse(new Uri(_baseUrl + $"/options/{prodOption.OptionId}/values"),
                                Token));
                        color = options.Find(colorOption => colorOption.Id == option.option_value_id).Label;
                        break;
                    }
                }
                catch
                {
                }
            }
        }

        public string GetStateByName(string name)
        {
            switch (name.ToUpper())
            {
                case "ALABAMA":
                    return "AL";

                case "ALASKA":
                    return "AK";

                case "AMERICAN SAMOA":
                    return "AS";

                case "ARIZONA":
                    return "AZ";

                case "ARKANSAS":
                    return "AR";

                case "CALIFORNIA":
                    return "CA";

                case "COLORADO":
                    return "CO";

                case "CONNECTICUT":
                    return "CT";

                case "DELAWARE":
                    return "DE";

                case "DISTRICT OF COLUMBIA":
                    return "DC";

                case "FEDERATED STATES OF MICRONESIA":
                    return "FM";

                case "FLORIDA":
                    return "FL";

                case "GEORGIA":
                    return "GA";

                case "GUAM":
                    return "GU";

                case "HAWAII":
                    return "HI";

                case "IDAHO":
                    return "ID";

                case "ILLINOIS":
                    return "IL";

                case "INDIANA":
                    return "IN";

                case "IOWA":
                    return "IA";

                case "KANSAS":
                    return "KS";

                case "KENTUCKY":
                    return "KY";

                case "LOUISIANA":
                    return "LA";

                case "MAINE":
                    return "ME";

                case "MARSHALL ISLANDS":
                    return "MH";

                case "MARYLAND":
                    return "MD";

                case "MASSACHUSETTS":
                    return "MA";

                case "MICHIGAN":
                    return "MI";

                case "MINNESOTA":
                    return "MN";

                case "MISSISSIPPI":
                    return "MS";

                case "MISSOURI":
                    return "MO";

                case "MONTANA":
                    return "MT";

                case "NEBRASKA":
                    return "NE";

                case "NEVADA":
                    return "NV";

                case "NEW HAMPSHIRE":
                    return "NH";

                case "NEW JERSEY":
                    return "NJ";

                case "NEW MEXICO":
                    return "NM";

                case "NEW YORK":
                    return "NY";

                case "NORTH CAROLINA":
                    return "NC";

                case "NORTH DAKOTA":
                    return "ND";

                case "NORTHERN MARIANA ISLANDS":
                    return "MP";

                case "OHIO":
                    return "OH";

                case "OKLAHOMA":
                    return "OK";

                case "OREGON":
                    return "OR";

                case "PALAU":
                    return "PW";

                case "PENNSYLVANIA":
                    return "PA";

                case "PUERTO RICO":
                    return "PR";

                case "RHODE ISLAND":
                    return "RI";

                case "SOUTH CAROLINA":
                    return "SC";

                case "SOUTH DAKOTA":
                    return "SD";

                case "TENNESSEE":
                    return "TN";

                case "TEXAS":
                    return "TX";

                case "UTAH":
                    return "UT";

                case "VERMONT":
                    return "VT";

                case "VIRGIN ISLANDS":
                    return "VI";

                case "VIRGINIA":
                    return "VA";

                case "WASHINGTON":
                    return "WA";

                case "WEST VIRGINIA":
                    return "WV";

                case "WISCONSIN":
                    return "WI";

                case "WYOMING":
                    return "WY";
            }

            throw new Exception("State Not Available");
        }


    }
}