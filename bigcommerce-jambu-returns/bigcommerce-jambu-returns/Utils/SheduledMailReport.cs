﻿using System;
using System.Configuration;
using Quartz;
using Quartz.Impl;

namespace bigcommerce_jambu_returns.Utils
{

    public static class SheduledMailReport
    {
        //private static IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
        public static void Start(IScheduler scheduler)
        {
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<EmailJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                  (s =>
                     s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(20, 18))
                  )
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }

    public class EmailJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            ReportHelper helper = new ReportHelper(ConfigurationManager.ConnectionStrings["ReturnsConectionString"].ConnectionString);
            helper.GenerateReport();
        }
    }
}