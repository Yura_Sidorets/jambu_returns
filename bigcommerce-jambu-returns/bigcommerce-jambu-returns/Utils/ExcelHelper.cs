﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using bigcommerce_jambu_returns.Models;
using ClosedXML.Excel;

namespace bigcommerce_jambu_returns.Utils
{
    public static class ExcelHelper
    {
        public static void CreateExcel(ref Dictionary<string, ReturnedObjectStats> stats, ref ReturnedObjectStats totalStats, string start, string end)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {

            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Sheet");
            worksheet.Cell("A1").Value = "Itemno";
            worksheet.Cell("B1").Value = "Descript";
            worksheet.Cell("C1").Value = "Season";
            worksheet.Cell("D1").Value = "Gender";
            worksheet.Cell("E1").Value = "Return Rate";
            worksheet.Cell("F1").Value = "Return Rate $";
            worksheet.Cell("G1").Value = "Ship Qty";
            worksheet.Cell("H1").Value = "Ship $";
            worksheet.Cell("I1").Value = "Return Qty";
            worksheet.Cell("J1").Value = "Return $";
            worksheet.Cell("K1").Value = "Net Qty";
            worksheet.Cell("L1").Value = "Net $";
            worksheet.Cell("M1").Value = "Too Big";
            worksheet.Cell("N1").Value = "Too Small";
            worksheet.Cell("O1").Value = "Too Wide";
            worksheet.Cell("P1").Value = "Too Narrow";
            worksheet.Cell("Q1").Value = "Did Not Like";
            worksheet.Cell("R1").Value = "Not Comforatble";
            worksheet.Cell("S1").Value = "Color Not Expected";
            worksheet.Cell("T1").Value = "Ordered two sizes and returning one";
            worksheet.Cell("U1").Value = "Did not meet value expectations";
            worksheet.Cell("V1").Value = "Wrong shoe shipped";
            worksheet.Cell("W1").Value = "Received too late";
            worksheet.Cell("X1").Value = "Defective";
            worksheet.Cell("Y1").Value = "Other";

            worksheet.Cell("E2").Value = totalStats.ReturnRate + "%";
            worksheet.Cell("F2").Value = totalStats.ReturnRateCost + "%";
            worksheet.Cell("G2").Value = totalStats.ShipQty;
            worksheet.Cell("H2").Value = "$"+totalStats.ShipCost;
            worksheet.Cell("I2").Value = totalStats.ReturnQty;
            worksheet.Cell("J2").Value = "$"+totalStats.ReturnCost;
            worksheet.Cell("K2").Value = totalStats.NetQty;
            worksheet.Cell("L2").Value = totalStats.NetCost;
            worksheet.Cell("M2").Value = totalStats.Reason[Reason.TooBig];
            worksheet.Cell("N2").Value = totalStats.Reason[Reason.TooSmall];
            worksheet.Cell("O2").Value = totalStats.Reason[Reason.TooWide];
            worksheet.Cell("P2").Value = totalStats.Reason[Reason.TooNarrow];
            worksheet.Cell("Q2").Value = totalStats.Reason[Reason.DidNotLike];
            worksheet.Cell("R2").Value = totalStats.Reason[Reason.NotComfortable];
            worksheet.Cell("S2").Value = totalStats.Reason[Reason.ColorNotAsExpected];
            worksheet.Cell("T2").Value = totalStats.Reason[Reason.TwoSizes];
            worksheet.Cell("U2").Value = totalStats.Reason[Reason.Expectations];
            worksheet.Cell("V2").Value = totalStats.Reason[Reason.WrongShoe];
            worksheet.Cell("W2").Value = totalStats.Reason[Reason.TooLate];
            worksheet.Cell("X2").Value = totalStats.Reason[Reason.Defective];
            worksheet.Cell("Y2").Value = totalStats.Reason[Reason.Other];
            worksheet.RangeUsed().Sort(5, XLSortOrder.Descending);

            byte i = 3;
            foreach (var stat in stats)
            {
                worksheet.Cell("A"+i).Value = stat.Value.ItemNo;
                worksheet.Cell("B"+i).Value = stat.Value.Description;
                worksheet.Cell("C"+i).Value = "";
                worksheet.Cell("D"+i).Value = "";
                worksheet.Cell("E"+i).Value = stat.Value.ReturnRate + "%";
                worksheet.Cell("F"+i).Value = stat.Value.ReturnRateCost + "%";
                worksheet.Cell("G"+i).Value = stat.Value.ShipQty;
                worksheet.Cell("H"+i).Value = "$" + stat.Value.ShipCost;
                worksheet.Cell("I"+i).Value = stat.Value.ReturnQty;
                worksheet.Cell("J"+i).Value = "$" + stat.Value.ReturnCost;
                worksheet.Cell("K"+i).Value = stat.Value.NetQty;
                worksheet.Cell("L"+i).Value = stat.Value.NetCost;

                
                worksheet.Cell("M"+i).Value = stat.Value.Reason[Reason.TooBig];
                worksheet.Cell("N"+i).Value = stat.Value.Reason[Reason.TooSmall];
                worksheet.Cell("O"+i).Value = stat.Value.Reason[Reason.TooWide];
                worksheet.Cell("P"+i).Value = stat.Value.Reason[Reason.TooNarrow];
                worksheet.Cell("Q"+i).Value = stat.Value.Reason[Reason.DidNotLike];
                worksheet.Cell("R"+i).Value = stat.Value.Reason[Reason.NotComfortable];
                worksheet.Cell("S"+i).Value = stat.Value.Reason[Reason.ColorNotAsExpected];
                worksheet.Cell("T"+i).Value = stat.Value.Reason[Reason.TwoSizes];
                worksheet.Cell("U"+i).Value = stat.Value.Reason[Reason.Expectations];
                worksheet.Cell("V"+i).Value = stat.Value.Reason[Reason.WrongShoe];
                worksheet.Cell("W"+i).Value = stat.Value.Reason[Reason.TooLate];
                worksheet.Cell("X"+i).Value = stat.Value.Reason[Reason.Defective];
                worksheet.Cell("Y"+i).Value = stat.Value.Reason[Reason.Other];
                i += 1;
            }
                workbook.SaveAs(memoryStream);
                memoryStream.Position = 0;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                //Create a FTP Request Object and Specfiy a Complete Path
                FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create("ftp://66.234.228.199:21/Returns/" + "SalesReturnReport_" + start.Replace("/","")+"_"+end.Replace("/","")+ "_" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".xlsx");
                reqObj.Credentials = new NetworkCredential("ecommerce", "ecommerce!%");
                //Call A FileUpload Method of FTP Request Object
                reqObj.Method = WebRequestMethods.Ftp.UploadFile;

                reqObj.EnableSsl = false;
                reqObj.UsePassive = true;
                reqObj.UseBinary = true;
                reqObj.KeepAlive = true;
                reqObj.Proxy = null;

                //If you want to access Resourse Protected,give UserName and PWD

                // Copy the contents of the file to the byte array.
                memoryStream.Seek(0, SeekOrigin.Begin);

                byte[] fileContents = memoryStream.ToArray();
                reqObj.ContentLength = fileContents.Length;
                //Upload File to FTPServer
                Stream requestStream = reqObj.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();
                FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
                response.Close();

            }
        }
    }
}