﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bigcommerce_jambu_returns.Models;

namespace bigcommerce_jambu_returns.Utils
{
    public interface IOlapicHelper
    {
        void PostFeed(string connectionString);
        OlapicFeed.Feed GenerateFeed(string connectionString);
    }
}
