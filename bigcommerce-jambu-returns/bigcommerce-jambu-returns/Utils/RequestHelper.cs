﻿using System;
using System.IO;
using System.Net;
using bigcommerce_jambu_returns.Properties;

namespace bigcommerce_jambu_returns.Utils
{
    public class RequestHelper
    {
        public static string GetResponse(Uri uri, string token)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
            req.AllowAutoRedirect = true;
            req.ContentType = "application/json";
            req.Accept = "application/json";
            req.Method = "GET";

            //string authValue = Convert.ToBase64String(Encoding.Default.GetBytes("admin:" + ""));

            req.Headers.Add("X-Auth-Client", Settings.Default.ClientId);
            req.Headers.Add("X-Auth-Token", token);
            //req.Headers.Add("Authorization", authValue);

            string jsonResponse = null;
            using (WebResponse resp = req.GetResponse())
            {
                if (req.HaveResponse)
                {
                    using (var reader = new StreamReader(resp.GetResponseStream()))
                    {
                        jsonResponse = reader.ReadToEnd();
                    }
                }
            }
            return jsonResponse;
        }
    }
}