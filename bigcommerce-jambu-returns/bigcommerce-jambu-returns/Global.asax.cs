﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using bigcommerce_jambu_returns.Utils;

namespace bigcommerce_jambu_returns
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

           
           // AddTask("email", 3600); // 86400
        }
        private static CacheItemRemovedCallback OnCacheRemove = null;

        private void AddTask(string name, int seconds)
        {
            OnCacheRemove = new CacheItemRemovedCallback(CacheItemRemoved);
            HttpRuntime.Cache.Insert(name, seconds, null,
                DateTime.Now.AddSeconds(seconds), Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, OnCacheRemove);
        }

        public void CacheItemRemoved(string k, object v, CacheItemRemovedReason r)
        {
            // do stuff here if it matches our taskname, like WebRequest
            // re-add our task so it recurs
           if (DateTime.Now.Hour == 22)
            {
                try
                {

                //ReportHelper helper =
                //    new ReportHelper(ConfigurationManager.ConnectionStrings["ReturnsConectionString"].ConnectionString);
                //helper.GenerateReport();
                //AddTask(k, 3600);
                }
                catch
                {
                    AddTask(k, 120);
                }
            }
            else
            {
                AddTask(k, Convert.ToInt32(v));
            }
        }
    }
}
