﻿using System;
using bigcommerce_jambu_returns.Cryptography;
using bigcommerce_returns.Data;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns
{
    public static class StoreProvider
    {
        public static Store ReadStore(string signedPayload, string clientSecret)
        {
            if (!signedPayload.Contains("."))
                throw new ArgumentException("SignedPayload not in correct format.");

            var parts = signedPayload.Split('.');
            var encodedJsonString = parts[0];
            var json = Base64Cryptography.Decode(encodedJsonString);
            var store = JsonConvert.DeserializeObject<Store>(json);
            var encodedHmacSignature = parts[1];
            var hmacSignature = Base64Cryptography.Decode(encodedHmacSignature);
            var signature = HMACCryptography.Decode(clientSecret, json);

            if (!string.Equals(hmacSignature, signature, StringComparison.CurrentCultureIgnoreCase))
                throw new InvalidOperationException("Invalid hmac signature");

            return store;
        }
    }
}