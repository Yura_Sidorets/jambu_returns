﻿var ReturnsView = BaseView.extend({
     initialize: function() {
         var a = new ConfirmationDialog("#custom-view-delete", { content: "Are you sure you want to delete the custom view?", autobind: !1 });
         
         $("#custom-view-delete").bind("click", function (b) { $("#custom-view-id").val() ? a.display(b) : new AlertDialog("Please choose a view to delete") }),
         $("#custom-view-delete").bind("confirm.confirmation", function () { window.location.href = "/admin/index.php?ToDo=deleteCustomReturnSearch&searchId=" + $("#custom-view-id").val() }),
         $(".returnStatusSelect").each(function () {
             var a = new ConfirmationDialog($(this), { autobind: !1 });
             $(this)
                     .bind("change",
                         function(b) { a.display(b) }),
                 $(this)
                     .bind("before.confirmation",
                         function() {
                             a._content = 'Are you sure you want to change status from "' +
                                 $(this).data("initial-status") +
                                 '" to "' +
                                 $(":selected", $(this)).text() +
                                 '"'
                         }),
                 $(this)
                     .bind("cancel.confirmation",
                         function() {
                             $(this).val($(this).data("initial-status"))
                         }),
                 $(this)
                     .bind("confirm.confirmation",
                         function() {
                             $.ajax({
                                 type: "POST",
                                 url: "api/orders/updateStatus",
                                 data: {
                                     returnId: $(this).data("returnid"),
                                     returnStatus: $(this).val()
                                 },
                                 success: function() {
                                     $('#status-modal-ok').modal('show');
                                 },
                                 error: function() {
                                     $('#status-modal-fail').modal('show');
                                 }
                             });
                         });
         }),
         $(".issueReturnCredit").each(function () { new ConfirmationDialog($(this), { content: "Are you sure you want to issue this customer with a store credit?<p>This customer will receive a store credit of " + $(this).data("return-amount") + " for this return." }) })
     }, events: { "click #custom-view-select": "customViewSelect" },
     customViewSelect: function () { var a = $("#custom-view-id").val(); null != a ? document.location.href = "/admin/index.php?ToDo=customReturnSearch&searchId=" + a : new AlertDialog("Please choose a custom view first.", "select-custom-view") }
});
ReturnsView.mixin(RowSelection), ReturnsView.mixin(DeleteSelectedRows), new ReturnsView({ el: $("#returns-index") });