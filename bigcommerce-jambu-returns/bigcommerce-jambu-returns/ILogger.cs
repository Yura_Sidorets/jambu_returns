﻿namespace bigcommerce_jambu_returns
{
    public interface ILogger
    {
        void Error(string message,string module);
        void Info(string message, string module);
        void Warning(string message, string module);
    }
}