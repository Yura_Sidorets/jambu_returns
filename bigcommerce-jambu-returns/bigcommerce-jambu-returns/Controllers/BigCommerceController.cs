﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.WebPages;
using bigcommerce_jambu_returns.Models;
using bigcommerce_jambu_returns.Properties;
using bigcommerce_jambu_returns.Utils;
using bigcommerce_jambu_returns.ViewModels;
using bigcommerce_jambu_returns.WebClients;
using bigcommerce_returns.Data;
using Newtonsoft.Json;
using Ninject;
using PagedList.Mvc;
using PagedList;
using bigcommerce_returns.Repositories;

namespace bigcommerce_jambu_returns.Controllers
{
    public class BigCommerceController : Controller
    {
        [Inject]
        public IBigCommerceWebClient BigCommerceWebClient { get; set; }

        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        [Inject]
        public IReturnRepository ReturnsRepository { get; set; }

        [Inject]
        public IConfigurationRepository ConfigurationRepository { get; set; }

        [Inject]
        public ILogger Logger { get; set; }

        /// <summary>
        /// Auth enpoint for bigcommerce
        /// </summary>
        /// <param name="request">Reqest with merchant token, code and context</param>
        /// <returns></returns>
        public async Task<ActionResult> Auth([FromBody] AuthRequest request)
        {
            try
            {
                var receiveTokenRequest = new ReceiveTokenRequest
                {
                    ClientId = Settings.Default.ClientId,
                    ClientSecret = Settings.Default.ClientSecret,
                    Code = request.Code,
                    Scope = request.Scope,
                    RedirectUri = "https://ecommerce.vidagroup.com/jambuReturns/BigCommerce/auth",
                    Context = request.Context
                };

                var result = await BigCommerceWebClient.ReceiveTokenAsync(receiveTokenRequest);
                var merchant = MerchantRepository.Get(result.User.Id);

                if (merchant == null)
                {
                    var storeHash = result.Context.Split('/').Last();
                    merchant = new Merchant { Id = result.User.Id, AccessToken = result.AccessToken, StoreHash = storeHash, WebApiClientKey = Settings.Default.ClientId };
                    MerchantRepository.Add(merchant);
                }
                else
                {
                    MerchantRepository.UpdateAccessToken(int.Parse(merchant.Id), result.AccessToken);
                }

                return View("Auth");
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }

        }

        public ActionResult Load(OrderSearchRequest value)
        {
            try
            {
                int pageSize = 20;
                var pagenum = Request.QueryString.Get("page");
                int? page = 1;
                if (pagenum != null)
                {
                    page = int.Parse(pagenum);
                }

                int pageNumber = (page ?? 1);
                int? offset = pageSize * (page - 1);
                if (page == 1)
                {
                    offset = 0;
                }


                var signedPayload = "";
                if (Request.QueryString.Count > 0)
                    signedPayload = Request.QueryString.Get("signed_payload");
                if (!string.IsNullOrEmpty(signedPayload))
                {
                    //Test for store users
                    MerchantRepository.UpdateSignedPayload(Settings.Default.ClientId, signedPayload);
                }
                else
                {
                    signedPayload = MerchantRepository.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload;
                }
                ViewBag.payload = MerchantRepository.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload;
                var store = StoreProvider.ReadStore(signedPayload, Settings.Default.ClientSecret);
                string url = $"https://api.bigcommerce.com/stores/" + MerchantRepository.Get(store.Owner.Id).StoreHash + "/v2/store";
                var response = RequestHelper.GetResponse(new Uri(url), MerchantRepository.Get(store.Owner.Id).AccessToken);
                ViewBag.storeUrl = JsonConvert.DeserializeObject<StoreInfo>(response).SecureUrl;

                ReturnViewModel viewModel = new ReturnViewModel();
                IEnumerable<Return> returnOrders;
                int count = 0;
                if (!value.searchQuery.IsEmpty())
                {
                    
                    returnOrders = ReturnsRepository.Search(value.searchQuery, offset, pageSize);
                    viewModel.SearchString = value.searchQuery;
                    count = ReturnsRepository.GetSearchResultsCount(value.searchQuery);
                }
                else
                {
                    //Get All Returns
                    returnOrders = ReturnsRepository.GetReturnsWithPagination(offset, pageSize);
                    count = ReturnsRepository.GetReturnsCount();
                }
                var enumerable = returnOrders as IList<Return> ?? returnOrders.ToList();
                if (enumerable.Any())
                {
                //    foreach (var order in enumerable)
                //    {
                //        if (viewModel.Products.Find(x => x.Sku == order.SKU) == null)
                //        {
                //            var prod = ReturnsRepository.GetProductBySku(order.SKU);
                //            if (prod == null)
                //            {
                //                prod = ReturnsRepository.GetProductByName(order.ProductName);
                //                if (prod == null)
                //                {
                //                    prod = ReturnsRepository.GetProduct(order.ProductId);
                //                }
                //            }
                //            viewModel.Products.Add(prod);
                //        }
                //        viewModel.ReturnOrders.Add(order);
                //    }
                //    viewModel.Products.Reverse();
                //    viewModel.ReturnOrders.Reverse();
                    viewModel.ReturnOrders = enumerable.ToList();
                    for (int i = 0; i < count; i++)
                    {
                        viewModel.Products.Add(i);
                    }
                    viewModel.PagedProducts = (PagedList<int>) viewModel.Products.ToPagedList(pageNumber, pageSize);
                    viewModel.Products = null;

                    return View("Dashboard", viewModel);
                }
                else
                {
                    return View("Dashboard", new ReturnViewModel());
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public ActionResult Sort([FromBody] string value)
        {
            try
            {
                int pageSize = 20;
                var pagenum = Request.QueryString.Get("page");
                int? page = 1;
                if (pagenum != null)
                {
                    page = int.Parse(pagenum);
                }

                int pageNumber = (page ?? 1);

                var signedPayload = MerchantRepository.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload;
                ViewBag.payload = MerchantRepository.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload; 

                var store = StoreProvider.ReadStore(signedPayload, Settings.Default.ClientSecret);
                string url = $"https://api.bigcommerce.com/stores/" + MerchantRepository.Get(store.Owner.Id).StoreHash + "/v2/store";
                var response = RequestHelper.GetResponse(new Uri(url), MerchantRepository.Get(store.Owner.Id).AccessToken);
                ViewBag.storeUrl = JsonConvert.DeserializeObject<StoreInfo>(response).SecureUrl;

                ReturnViewModel viewModel = new ReturnViewModel();
                IEnumerable<Return> returnOrders;
                //Get All Returns
                returnOrders = ReturnsRepository.GetReturnsWithPagination(pageSize * page, pageSize);
                var enumerable = returnOrders as IList<Return> ?? returnOrders.ToList();
                if (enumerable.Any())
                {
                    //foreach (var order in enumerable)
                    //{
                    //    if (viewModel.Products.Find(x => x.ProductId == order.ProductId) == null)
                    //    {
                    //        var prod = ReturnsRepository.GetProductBySku(order.SKU);
                    //        if (prod == null)
                    //        {
                    //            prod = ReturnsRepository.GetProductByName(order.ProductName);
                    //            if (prod == null)
                    //            {
                    //                prod = ReturnsRepository.GetProduct(order.ProductId);
                    //            }
                    //        }
                    //        viewModel.Products.Add(prod);
                    //    }
                    //    viewModel.ReturnOrders.Add(order);

                    //}
                    viewModel.ReturnOrders = enumerable.ToList();
                    var count = ReturnsRepository.GetReturnsCount();
                    for (int i = 0; i < count; i++)
                    {
                        viewModel.Products.Add(i);
                    }
                    viewModel.PagedProducts = (PagedList<int>)viewModel.Products.ToPagedList(pageNumber, pageSize);
                    viewModel.Products = null;

                    return View("Dashboard", viewModel);
                }
                else
                {
                    return View("Dashboard", new ReturnViewModel());
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }


        public ActionResult Uninstall()
        {
            try
            {
                if (Request.QueryString.Count != 1)
                    throw new InvalidOperationException();

                var signedPayload = Request.QueryString[0];
                var store = StoreProvider.ReadStore(signedPayload, Settings.Default.ClientSecret);

                MerchantRepository.Remove(store.Owner.Id);
                return View("Auth");
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        [System.Web.Http.Route("user/remove")]
        public void RemoveUser()
        {
            
        }

        #region Utils
        public ActionResult Config()
        {
            try
            {
                var signedPayload = "";
                if (Request.QueryString.Count > 0)
                    signedPayload = Request.QueryString[0];
                if (signedPayload != "")
                {
                    MerchantRepository.UpdateSignedPayload(Settings.Default.ClientId, signedPayload);
                }
                else
                {
                    signedPayload = MerchantRepository.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload;
                    //signedPayload = MerchantRepository.GetSignedPayload();
                }
                ViewBag.payload = MerchantRepository.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload; //MerchantRepository.GetSignedPayload();
                var store = StoreProvider.ReadStore(signedPayload, Settings.Default.ClientSecret);
                var config = ConfigurationRepository.GetApplicationConfiguration(int.Parse(store.Owner.Id));

                if (config.Address == null || config.UpsCredentials == null || config.FtpCredentials == null)
                {
                    config = new ApplicationConfiguration
                    {
                        Address = new Address(),
                        UpsCredentials =new UpsCredentials(),
                        FtpCredentials = new FtpCredentials()
                    };
                }
                return View(config);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw; //return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            }
        }

        [System.Web.Http.Route("BigCommerce/label")]
        public FileResult Label([FromBody]string hash)
        {
            try
            {
                var file = System.IO.File.ReadAllBytes(Path.Combine(System.Web.HttpContext.Current.
       Server.MapPath("~/Labels/"),  $"{hash}.gif"));
                using (var mem = new MemoryStream(file))
                {
                    return File(mem.ToArray(), "image/gif", hash+".gif");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
        #endregion
    }
}