﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Mvc;
using bigcommerce_jambu_returns.Models;
using bigcommerce_jambu_returns.Utils;

namespace bigcommerce_jambu_returns.Controllers
{
    public class ReportController : Controller
    {        // GET: Report
        [System.Web.Http.Route("BigCommerce/Report")]
        public ActionResult Index()
        {
            var signedPayload = Request.QueryString[0];
            ViewBag.payload = signedPayload;
            return View("Report");
        }

        [System.Web.Mvc.HttpPost]
        public HttpResponseMessage CreateReport([FromBody] SalesReturnReportParams salesReturnParams)
        {
            var response = new HttpRequestMessage().CreateResponse(System.Net.HttpStatusCode.OK);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            response.Headers.Add("Access-Control-Allow-Methods", "POST");

            try
            {
                ReportHelper.GenerateSalesReport(DateTime.Parse(salesReturnParams.StartDate),DateTime.Parse(salesReturnParams.EndDate), ConfigurationManager.ConnectionStrings["ReturnsConectionString"].ConnectionString);

                var resp = new SalesReturnReportResponse
                {
                    Success = 1
                };
                response.Content = new ObjectContent<SalesReturnReportResponse>(resp, new JsonMediaTypeFormatter(),
                          "application/json");
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
    }
}