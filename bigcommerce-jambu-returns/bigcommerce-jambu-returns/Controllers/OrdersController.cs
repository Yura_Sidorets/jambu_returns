﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using bigcommerce_jambu_returns.Models;
using bigcommerce_jambu_returns.Properties;
using bigcommerce_jambu_returns.UPS_Shipping;
using bigcommerce_jambu_returns.Utils;
using bigcommerce_returns.Data;
using bigcommerce_returns.Repositories;
using Ninject;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.WebPages;

namespace bigcommerce_jambu_returns.Controllers
{

    public class OrdersController : ApiController
    {
        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }

        [Inject]
        public IReturnRepository ReturnsRepository { get; set; }

        [Inject]
        public IConfigurationRepository ConfigurationRepository { get; set; }

        [Inject]
        public ILogger Logger { get; set; }

        // POST api/orders
        [System.Web.Http.Route("api/returns/create")]
        [System.Web.Mvc.HttpPost]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage Post(FormData value)
        {
            var signedPayload = MerchantRepository.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload;
            var store = StoreProvider.ReadStore(signedPayload, Settings.Default.ClientSecret);
            OrdersHelper ordersHelper = new OrdersHelper(MerchantRepository.Get(store.Owner.Id).StoreHash,
                MerchantRepository.Get(store.Owner.Id).AccessToken);
            var merchantConfig = ConfigurationRepository.GetApplicationConfiguration(int.Parse(store.Owner.Id));

            var response = Request.CreateResponse(System.Net.HttpStatusCode.OK);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            response.Headers.Add("Access-Control-Allow-Methods", "POST");
            try
            {
                bool guestUser;
                if (value == null)
                {
                    throw new Exception("Error in recieved data");
                }
                if (value.customer == null)
                {
                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                    {
                        result = 0,
                        error = new Error()
                        {
                            message = "Customer does not provided."
                        }
                    }, new JsonMediaTypeFormatter(), "application/json");
                    return response;
                }
                if (value.order_id == 0)
                {
                    throw new Exception("Order ID does not provided.");
                }

                #region Check Customer

                List<Customer> customerToReturn = new List<Customer>();
                try
                {
                    if (value.customer.Contains("@"))
                    {
                        try
                        {
                            //Get Customer by email
                            customerToReturn = ordersHelper.GetCustomers(value.customer);
                            if (customerToReturn == null || customerToReturn.Count == 0)
                            {
                                guestUser = true;
                            }
                        }
                        catch
                        {

                            var order = ordersHelper.GetOrder(value.order_id.ToString());
                            if (order != null)
                            {
                                if (order.CustomerId != 0)
                                {
                                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                                    {
                                        result = 0,
                                        error = new Error()
                                        {
                                            message = "Error in customer parameter"
                                        }
                                    }, new JsonMediaTypeFormatter(), "application/json");
                                    return response;
                                }
                                guestUser = true;
                            }
                        }

                    }
                    else
                    {
                        //Get Customer by ID
                        var customer = ordersHelper.GetCustomerById(value.customer);
                        if (customer != null)
                            customerToReturn.Add(customer);

                        if (customerToReturn.Count == 0)
                        {
                            response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                            {
                                result = 0,
                                error = new Error()
                                {
                                    message = "Customer does not exists."
                                }
                            }, new JsonMediaTypeFormatter(), "application/json");
                            return response;
                        }
                    }
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                    {
                        result = 0,
                        error = new Error()
                        {
                            message = "Error in customer parameter"
                        }
                    }, new JsonMediaTypeFormatter(), "application/json");
                    return response;
                }

                #endregion

                #region Check Order

                //Get order to return
                ReturnRequestOrder orderToReturn;
                try
                {
                    orderToReturn = ordersHelper.GetOrder(value.order_id.ToString());
                }
                catch
                {
                    //check orders id if lower 34000 map to range 130000 over 10000
                    if (value.order_id < 34000)
                    {
                        value.order_id = int.Parse(ReturnsRepository.GetOrderIdMapping(value.order_id.ToString()));
                        try
                        {
                            orderToReturn = ordersHelper.GetOrder(value.order_id.ToString());
                        }
                        catch
                        {
                            response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                            {
                                result = 0,
                                error = new Error()
                                {
                                    message =
                                        "We’re sorry, we cannot locate that order. Please call customer service at 1-877-541-5337 and they will assist you."
                                }
                            }, new JsonMediaTypeFormatter(), "application/json");
                            return response;
                        }

                    }
                    else
                    {
                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message =
                                    "We’re sorry, we cannot locate that order. Please call customer service at 1-877-541-5337 and they will assist you."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                }

                if (orderToReturn == null)
                {

                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                    {
                        result = 0,
                        error = new Error()
                        {
                            message =
                                "We’re sorry, we cannot locate that order. Please call customer service at 1-877-541-5337 and they will assist you."
                        }
                    }, new JsonMediaTypeFormatter(), "application/json");
                    return response;
                }
                Customer customerInOrder = new Customer();
                if (orderToReturn.CustomerId != 0)
                {
                    customerInOrder = ordersHelper.GetCustomerById(orderToReturn.CustomerId.ToString());
                    guestUser = false;
                }
                else
                {
                    guestUser = true;
                }

                if (!guestUser)
                {
                    //Check order's belonging
                    if (value.customer.Contains("@") &&
                        !value.customer.ToLowerInvariant().Equals(customerInOrder.Email.ToLowerInvariant()))
                    {
                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message = "Order does not belongs to this customer."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                    if (!value.customer.Contains("@") && !value.customer.Equals(orderToReturn.CustomerId.ToString()))
                    {

                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message = "Order does not belongs to this customer."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                }
                else
                {
                    if (!value.customer.Contains("@") && !value.customer.Equals(orderToReturn.CustomerId.ToString()))
                    {

                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message = "Order does not belongs to this customer."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                }

                if (orderToReturn.Status == "Shipped" || orderToReturn.Status == "Completed")
                {
                    //Chech orded shipped no longer 30 days
                    if (DateTime.Parse(orderToReturn.DateCreated) - DateTime.Now < TimeSpan.FromDays(-60))
                    {

                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message =
                                    "You are not able to create return request because order is older than 30 days."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }

                    #endregion

                    //Check logged in user or not
                    if (value.return_action == null && value.return_qty == null && value.return_reason == null)
                    {
                        //return all is ok
                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 1,
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                    if (value.return_action == null || value.return_qty == null || value.return_reason == null)
                    {
                        //return error
                        throw new Exception("Error in parameters or not all provided");
                    }

                    //Get order shipping 
                    var orderShipping = ordersHelper.GetShippings(value.order_id.ToString());
                    if (orderShipping == null)
                    {
                        throw new Exception("No shipments found");
                    }

                    // Get information about store 
                    var storeResp = ordersHelper.GetStoreInfo();

                    //Get products from order
                    var productsToReturn = ordersHelper.GetProducts(value.order_id.ToString());

                    #region Check for already returned

                    //Check if return exists 
                    var alreadyReturnedOrder = ReturnsRepository.GetReturnByOrder(orderToReturn.Id);

                    var retAllitems = 0;
                    foreach (var order in alreadyReturnedOrder)
                    {
                        ReturnRequestProduct temp = new ReturnRequestProduct();
                        foreach (var returned in productsToReturn)
                        {
                            temp = returned;
                            if (returned.ProductId == order.ProductId ||
                                returned.ProductId == 0 && order.ProductName.Equals(returned.Name))
                            {
                                if (order.SKU.Equals(returned.Sku))
                                {
                                    returned.Quantity -= order.Quantity;
                                    break;
                                }
                            }

                        }
                        if (temp?.Quantity <= 0)
                        {
                            retAllitems++;
                        }

                    }

                    if (retAllitems == productsToReturn.Count)
                    {
                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message = "Order already returned."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }

                    #endregion

                    foreach (var prod in productsToReturn)
                    {
                        var firstOrDefault = value.return_qty.FirstOrDefault(x => x.item_id == prod.Id);
                        if (prod.Quantity < firstOrDefault?.quantity)
                        {
                            throw new Exception("Item quantity greater than can be returned");
                        }
                        if (firstOrDefault?.quantity <= 0)
                        {
                            throw new Exception("Item quantity can not be 0");
                        }
                    }

                    #region Get product Info

                    var shipments = new List<PrShipmentResponse>();
                    var packageWeight = 0.0;
                    //Get every product info to store in db
                    foreach (var prod in productsToReturn)
                    {
                        try
                        {
                            ordersHelper.GetProductInfo(prod.ProductId);
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                        }
                        var dataP = ReturnsRepository.GetProductBySku(prod.Sku);

                        if (dataP == null)
                        {

                            //Get info about product
                            try
                            {
                                dataP = ordersHelper.GetProductInfo(prod.ProductId);
                            }
                            catch
                            {
                                dataP = ReturnsRepository.GetProductByName(prod.Name);
                                if (dataP == null)
                                {
                                    if (prod.ProductId == 0)
                                    {
                                        dataP = new Product
                                        {
                                            Name = prod.Name,
                                            Price = prod.BasePrice,
                                            ProductId = prod.Id,
                                            Weight = prod.Weight,
                                            Sku = prod.Name
                                        };
                                    }

                                    else
                                    {
                                        dataP = new Product
                                        {
                                            Name = prod.Name,
                                            Price = prod.BasePrice,
                                            ProductId = prod.ProductId,
                                            Weight = prod.Weight,
                                            Sku = prod.Sku
                                        };
                                    }


                                }
                            }

                            //Define Size and Color
                            if (prod.ProductOptions.Count > 0)
                            {
                                dataP.Size = prod.ProductOptions[0].DisplayName.Contains("Size") ? prod.ProductOptions[0].DisplayValue : prod.ProductOptions[1].DisplayValue;

                                dataP.Color = prod.ProductOptions[1].DisplayName.Contains("Color") ? prod.ProductOptions[1].DisplayValue : prod.ProductOptions[0].DisplayValue;
                            }
                            else
                            {
                                dataP.Size = "";
                                dataP.Color = "";
                            }
                            if (!prod.Sku.IsEmpty())
                            {
                                dataP.Sku = prod.Sku;
                            }
                            packageWeight += double.Parse(dataP.Weight);
                            ReturnsRepository.AddProduct(dataP);
                        }
                        else
                        {
                            try
                            {
                                //Get info about product
                                dataP = ordersHelper.GetProductInfo(prod.ProductId);
                            }
                            catch
                            {

                                if (prod.ProductId == 0)
                                {
                                    dataP = new Product
                                    {
                                        Name = prod.Name,
                                        Price = prod.BasePrice,
                                        ProductId = prod.Id,
                                        Weight = prod.Weight,
                                        Sku = prod.Name
                                    };
                                }
                                else
                                {
                                    dataP = new Product
                                    {
                                        Name = prod.Name,
                                        Price = prod.BasePrice,
                                        ProductId = prod.ProductId,
                                        Weight = prod.Weight,
                                        Sku = prod.Sku
                                    };
                                }

                            }
                            if (prod.ProductOptions.Count > 0)
                            {
                                dataP.Size = prod.ProductOptions[0].DisplayName.Contains("Size") ? prod.ProductOptions[0].DisplayValue : prod.ProductOptions[1].DisplayValue;

                                dataP.Color = prod.ProductOptions[1].DisplayName.Contains("Color") ? prod.ProductOptions[1].DisplayValue : prod.ProductOptions[0].DisplayValue;
                            }
                            if (!prod.Sku.IsEmpty())
                            {
                                dataP.Sku = prod.Sku;
                            }
                            packageWeight += double.Parse(dataP.Weight);
                            ReturnsRepository.UpdateProduct(dataP);
                        }
                    }

                    #endregion

                    #region Shipment creation

                    //shipper address

                    //var state = storeAddress[2].Split(',')[1].Trim(' ').Split(' ')[0];
                    //var zip = storeAddress[2].Split(',')[1].Trim(' ').Split(' ')[1];
                    //var city = storeAddress[2].Split(',')[0];
                    // var storeAddress = storeResp.Address.Split('\n');

                    var shipToState = merchantConfig.Address.State;
                    var shipFromState = orderShipping[0].ShippingAddress.State;

                    if (orderShipping[0].ShippingAddress.Country == "United States")
                    {
                        //if (state.Length > 2)
                        //    state = ordersHelper.GetStateByName(state);
                        if (merchantConfig.Address.State.Length > 2)
                            shipToState = ordersHelper.GetStateByName(merchantConfig.Address.State);
                        if (orderShipping[0].ShippingAddress.State.Length > 2)
                            shipFromState = ordersHelper.GetStateByName(orderShipping[0].ShippingAddress.State);
                    }
                    var returns = new List<Return>();
                    orderShipping[0].ShippingAddress.Phone = " ";
                    if (orderShipping[0].ShippingAddress.Phone.Equals("0000000000") ||
                        orderShipping[0].ShippingAddress.Phone.IsEmpty() ||
                        orderShipping[0].ShippingAddress.Phone.Equals(" ") ||
                        orderShipping[0].ShippingAddress.Phone.Equals("000-000-0000"))
                        orderShipping[0].ShippingAddress.Phone = "877-541-5337";
                    try
                    {
                        shipments.Add(UPS.RequestShipment(new PrShipmentRequest()
                        {
                            #region UPSSecurity
                            UPSSecurity = new UPSSecurity()
                            {
                                ServiceAccessToken = new ServiceAccessToken()
                                {
                                    AccessLicenseNumber = merchantConfig.UpsCredentials.AccessLicenseNumber
                                    //"FD108E7EFCB62FA8"
                                },
                                UsernameToken = new UsernameToken()
                                {
                                    Username = merchantConfig.UpsCredentials.Email,
                                    Password = merchantConfig.UpsCredentials.Password
                                }
                            },

                            #endregion
                            #region ShipmentRequest
                            ShipmentRequest = new ShipmentRequest()
                            {
                                LabelSpecification = new LabelSpecification()
                                {
                                    HTTPUserAgent = "Mozilla/4.5",
                                    LabelImageFormat = new LabelImageFormat()
                                    {
                                        Code = "GIF",
                                        Description = "GIF"
                                    }
                                },

                                #region Request
                                Request = new Request()
                                {
                                    RequestOption = "nonvalidate",
                                    TransactionReference = new TransactionReference()
                                    {
                                        CustomerContext = ""
                                    }
                                },

                                #endregion
                                #region Shipment
                                Shipment = new Shipment()
                                {
                                    #region Package
                                    Package = new Package()
                                    {
                                        Description = "02",
                                        Dimensions = new Dimensions()
                                        {
                                            Height = "0",
                                            Length = "0",
                                            Width = "0",
                                            UnitOfMeasurement = new UnitOfMeasurement()
                                            {
                                                Code = "IN"
                                            }
                                        },
                                        PackageWeight = new PackageWeight()
                                        {
                                            Weight = packageWeight.ToString(),
                                            UnitOfMeasurement = new UnitOfMeasurement()
                                            {
                                                Code = "LBS"
                                            }
                                        },
                                        Packaging = new Packaging()
                                        {
                                            Code = "02"
                                        },
                                        ReferenceNumber = new ReferenceNumber()
                                        {
                                            Code = "PO",
                                            Value = orderToReturn.Id.ToString()
                                        }
                                    },

                                    #endregion
                                    #region Payment and Service
                                    PaymentInformation = new PaymentInformation()
                                    {
                                        ShipmentCharge = new ShipmentCharge()
                                        {
                                            BillShipper = new BillShipper()
                                            {
                                                AccountNumber = merchantConfig.UpsCredentials.AccountNumber
                                                //"2X2085" 
                                            },
                                            Type = "01"
                                        }
                                    },
                                    Service = new Service()
                                    {
                                        Code = "03",
                                        Description = "Ground"
                                    },

                                    #endregion
                                    #region Addresses
                                    ShipFrom = new ShipFrom()
                                    {
                                        Name =
                                            orderShipping[0].ShippingAddress.FirstName + " " +
                                            orderShipping[0].ShippingAddress.LastName,
                                        Address = new UPS_Shipping.Address()
                                        {
                                            City = orderShipping[0].ShippingAddress.City,
                                            AddressLine = orderShipping[0].ShippingAddress.Street1,
                                            CountryCode = orderShipping[0].ShippingAddress.CountryIso2,
                                            PostalCode = orderShipping[0].ShippingAddress.Zip,
                                            StateProvinceCode = shipFromState
                                        },
                                        Phone = new Phone()
                                        {
                                            Number = orderShipping[0].ShippingAddress.Phone
                                        }
                                    },
                                    ShipTo = new ShipTo()
                                    {
                                        Name = merchantConfig.Address.AccountName,
                                        Address = new UPS_Shipping.Address()
                                        {
                                            City = merchantConfig.Address.City,
                                            AddressLine = merchantConfig.Address.Address1,
                                            CountryCode = merchantConfig.Address.Country,
                                            PostalCode = merchantConfig.Address.Zip,
                                            StateProvinceCode = shipToState
                                        },
                                        Phone = new Phone()
                                        {
                                            Number = merchantConfig.Address.Telephone
                                        }
                                    },
                                    Shipper = new Shipper()
                                    {

                                        Name =
                                            orderShipping[0].ShippingAddress.FirstName + " " +
                                            orderShipping[0].ShippingAddress.LastName,
                                        Address = new UPS_Shipping.Address()
                                        {
                                            City = orderShipping[0].ShippingAddress.City,
                                            AddressLine = orderShipping[0].ShippingAddress.Street1,
                                            CountryCode = orderShipping[0].ShippingAddress.CountryIso2,
                                            PostalCode = orderShipping[0].ShippingAddress.Zip,
                                            StateProvinceCode = shipFromState
                                        },
                                        Phone = new Phone()
                                        {
                                            Number = orderShipping[0].ShippingAddress.Phone
                                        },
                                        ShipperNumber = merchantConfig.UpsCredentials.AccountNumber,

                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            #endregion
                        }));
                    }
                    catch (Exception e)
                    {
                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message = "During creating return request error has occurred.",
                                description = e.Message
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;

                    }

                    #endregion

                    var labelNumber =
                        FileHelper.SaveShippingLabel(
                            shipments[0].ShipmentResponse.ShipmentResults.PackageResults.ShippingLabel.GraphicImage,
                            orderToReturn.Id + int.Parse(value.order_id.ToString()) + DateTime.Now.ToString());

                    if (guestUser)
                    {
                        customerToReturn = new List<Customer>
                        {
                            new Customer()
                            {
                                FirstName = orderShipping[0].ShippingAddress.FirstName,
                                LastName = orderShipping[0].ShippingAddress.LastName
                            }
                        };
                    }

                    foreach (ReturnRequestProduct productToReturn in productsToReturn)
                    {
                        if (!(productToReturn?.Quantity > 0)) continue;
                        var firstOrDefault =
                            value.return_qty.FirstOrDefault(x => x.item_id == productToReturn.Id);
                        if (firstOrDefault == null) continue;
                        if (productToReturn.ProductId == 0)
                        {
                            productToReturn.ProductId = productToReturn.Id;
                        }
                        returns.Add(new Return
                        {
                            ProductId = productToReturn.ProductId,
                            OrderId = value.order_id,
                            Customer = customerToReturn[0].FirstName + " " + customerToReturn[0].LastName,
                            Date = DateTime.Now.ToShortDateString(),
                            Comment = value.return_comments,
                            Status = "Approved",
                            Reason = value.return_reason,
                            Action = value.return_action,
                            Notes = string.Empty,
                            Quantity = firstOrDefault.quantity,
                            ProductName = productToReturn.Name,
                            TrackingNumber =
                                shipments[0]?.ShipmentResponse.ShipmentResults.ShipmentIdentificationNumber,
                            ShippingLabel = labelNumber.ToString(),
                            SKU = productToReturn.Sku,
                        });
                    }
                    OrderResponse resp = new OrderResponse();

                    //Add orders to DB
                    foreach (var returnOrder in returns)
                    {
                        ReturnsRepository.Add(returnOrder);
                    }
                    resp.result = 1;
                    resp.shipping_info = new ShippingInfo
                    {
                        shipping_label = Url.Content("~/Labels/") + labelNumber + ".gif",
                        tracking_number = shipments[0]?.ShipmentResponse.ShipmentResults.ShipmentIdentificationNumber
                    };

                    //Send email with lable to customer
                    var mailController = new MailController();
                    mailController.SendLabel(resp.shipping_info.shipping_label, customerToReturn[0].Email);

                    response.Content = new ObjectContent<OrderResponse>(resp, new JsonMediaTypeFormatter(),
                        "application/json");
                    return response;
                }
                response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                {
                    result = 0,
                    error = new Error()
                    {
                        message =
                            "You are not able to create return request because order has been not shipped/completed yet"
                    }
                }, new JsonMediaTypeFormatter(), "application/json");
                return response;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                {
                    result = 0,
                    error = new Error()
                    {
                        message = "During creating return request error has occurred.",
                        description = e.Message
                    }
                }, new JsonMediaTypeFormatter(), "application/json");
                return response;

            }
        }

        [System.Web.Http.Route("api/returns/check")]
        [System.Web.Http.HttpPost]
        [EnableCors("*", "*", "*")]
        public HttpResponseMessage CheckReturn(FormData value)
        {
            var signedPayload = MerchantRepository.GetMerchantByClientKey(Settings.Default.ClientId).SignedPayload;
            var store = StoreProvider.ReadStore(signedPayload, Settings.Default.ClientSecret);
            OrdersHelper ordersHelper = new OrdersHelper(MerchantRepository.Get(store.Owner.Id).StoreHash,
                MerchantRepository.Get(store.Owner.Id).AccessToken);
            //var merchantConfig = ConfigurationRepository.GetApplicationConfiguration(int.Parse(store.Owner.Id));

            var response = Request.CreateResponse(System.Net.HttpStatusCode.OK);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            response.Headers.Add("Access-Control-Allow-Methods", "POST");
            try
            {
                bool guestUser;
                if (value == null)
                {
                    throw new Exception("Error in recieved data");
                }
                if (value.customer == null)
                {
                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                    {
                        result = 0,
                        error = new Error()
                        {
                            message = "Customer does not provided."
                        }
                    }, new JsonMediaTypeFormatter(), "application/json");
                    return response;
                }
                if (value.order_id == 0)
                {
                    throw new Exception("Order ID does not provided.");
                }

                List<Customer> customerToReturn = new List<Customer>();
                try
                {
                    if (value.customer.Contains("@"))
                    {
                        try
                        {
                            //Get Customer by email
                            customerToReturn = ordersHelper.GetCustomers(value.customer);
                            if (customerToReturn == null || customerToReturn.Count == 0)
                            {
                                guestUser = true;
                            }
                        }
                        catch
                        {

                            var order = ordersHelper.GetOrder(value.order_id.ToString());
                            if (order != null)
                            {
                                if (order.CustomerId != 0)
                                {
                                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                                    {
                                        result = 0,
                                        error = new Error()
                                        {
                                            message = "Error in customer parameter"
                                        }
                                    }, new JsonMediaTypeFormatter(), "application/json");
                                    return response;
                                }
                                guestUser = true;
                            }
                        }

                    }
                    else
                    {
                        //Get Customer by ID
                        var customer = ordersHelper.GetCustomerById(value.customer);
                        if (customer != null)
                            customerToReturn.Add(customer);

                        if (customerToReturn.Count == 0)
                        {
                            response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                            {
                                result = 0,
                                error = new Error()
                                {
                                    message = "Customer does not exists."
                                }
                            }, new JsonMediaTypeFormatter(), "application/json");
                            return response;
                        }
                    }
                }
                catch (Exception)
                {

                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                    {
                        result = 0,
                        error = new Error()
                        {
                            message = "Error in customer parameter"
                        }
                    }, new JsonMediaTypeFormatter(), "application/json");
                    return response;
                }


                //Get order to return
                ReturnRequestOrder orderToReturn;
                try
                {
                    orderToReturn = ordersHelper.GetOrder(value.order_id.ToString());
                }
                catch
                {
                    //check orders id if lower 34000 map to range 130000 over 10000
                    if (value.order_id < 34000)
                    {
                        value.order_id = int.Parse(ReturnsRepository.GetOrderIdMapping(value.order_id.ToString()));
                        try
                        {
                            orderToReturn = ordersHelper.GetOrder(value.order_id.ToString());
                        }
                        catch
                        {
                            response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                            {
                                result = 0,
                                error = new Error()
                                {
                                    message =
                                        "We’re sorry, we cannot locate that order. Please call customer service at 1-877-541-5337 and they will assist you."
                                }
                            }, new JsonMediaTypeFormatter(), "application/json");
                            return response;
                        }

                    }
                    else
                    {
                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message =
                                    "We’re sorry, we cannot locate that order. Please call customer service at 1-877-541-5337 and they will assist you."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                }

                if (orderToReturn == null)
                {
                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                    {
                        result = 0,
                        error = new Error()
                        {
                            message =
                                "We’re sorry, we cannot locate that order. Please call customer service at 1-877-541-5337 and they will assist you."
                        }
                    }, new JsonMediaTypeFormatter(), "application/json");
                    return response;
                }

                Customer customerInOrder = new Customer();
                if (orderToReturn.CustomerId != 0)
                {
                    customerInOrder = ordersHelper.GetCustomerById(orderToReturn.CustomerId.ToString());
                    guestUser = false;
                }
                else
                {
                    guestUser = true;
                }

                if (!guestUser)
                {
                    //Check order's belonging
                    if (value.customer.Contains("@") &&
                        !value.customer.ToLowerInvariant().Equals(customerInOrder.Email.ToLowerInvariant()))
                    {
                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message = "Order does not belongs to this customer."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                    if (!value.customer.Contains("@") && !value.customer.Equals(orderToReturn.CustomerId.ToString()))
                    {

                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message = "Order does not belongs to this customer."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                }
                else
                {
                    if (!value.customer.Contains("@") && !value.customer.Equals(orderToReturn.CustomerId.ToString()))
                    {

                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message = "Order does not belongs to this customer."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                }

                if (orderToReturn.Status == "Shipped" || orderToReturn.Status == "Completed")
                {
                    //Chech orded shipped no longer 30 days
                    if (DateTime.Parse(orderToReturn.DateCreated) - DateTime.Now < TimeSpan.FromDays(-30))
                    {

                        response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                        {
                            result = 0,
                            error = new Error()
                            {
                                message =
                                    "You are not able to create return request because order is older than 30 days."
                            }
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }

                    var orderShipping = ordersHelper.GetShippings(value.order_id.ToString());
                    if (orderShipping == null)
                    {
                        throw new Exception("No shipments found");
                    }

                    //Check logged in user or not
                    if (value.return_action == null && value.return_qty == null && value.return_reason == null)
                    {
                        //Get products from order
                        var productsToReturn = ordersHelper.GetProducts(value.order_id.ToString());

                        //Check if return exists 
                        var alreadyReturnedOrder = ReturnsRepository.GetReturnByOrder(orderToReturn.Id);


                        var retAllitems = 0;
                        foreach (var order in alreadyReturnedOrder)
                        {
                            ReturnRequestProduct temp = new ReturnRequestProduct();
                            foreach (var returned in productsToReturn)
                            {
                                temp = returned;
                                if (returned.ProductId != order.ProductId &&
                                    (returned.ProductId != 0 || !order.ProductName.Equals(returned.Name))) continue;
                                if (!order.SKU.Equals(returned.Sku)) continue;
                                returned.Quantity -= order.Quantity;
                                break;
                            }
                            if (temp?.Quantity <= 0)
                            {
                                retAllitems++;
                            }

                        }

                        if (retAllitems == productsToReturn.Count)
                        {
                            response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                            {
                                result = 0,
                                error = new Error()
                                {
                                    message = "Order already returned."
                                }
                            }, new JsonMediaTypeFormatter(), "application/json");
                            return response;
                        }

                        // Get information about store 
                        var storeResp = ordersHelper.GetStoreInfo();
                        var products = new List<ReturnProduct>();
                        foreach (var product in productsToReturn)
                        {
                            string thumbnail = null;
                            var price = Math.Round(double.Parse(product.BasePrice), 2).ToString("#.00");
                            var prodOptions = new List<Option>();
                            if (product.ProductOptions.Count > 0)
                            {
                                string color = null;
                                foreach (var opt in product.ProductOptions)
                                {
                                    prodOptions.Add(new Option {option = opt.DisplayName, value = opt.DisplayValue});
                                    if (opt.DisplayName == "Color")
                                    {
                                        color = opt.DisplayValue;
                                    }
                                }
                                var images = ordersHelper.ListProductImages(product.ProductId);
                                var firstOrDefault = images.FirstOrDefault(x => x.description.Equals(color));
                                if (firstOrDefault != null)
                                    thumbnail = firstOrDefault.thumbnail_url;
                                products.Add(new ReturnProduct
                                {
                                    options = prodOptions,
                                    name = product.Name,
                                    product_id = product.Id,
                                    max_qty = product.Quantity,
                                    price = storeResp.CurrencySymbol + price,
                                    thumbnail_url = thumbnail
                                });
                                continue;
                            }
                            products.Add(new ReturnProduct
                            {
                                name = product.Name,
                                product_id = product.Id,
                                max_qty = product.Quantity,
                                price = storeResp.CurrencySymbol + price,
                                thumbnail_url = null
                            });

                        }
                        //return all is ok
                        response.Content = new ObjectContent<OrderResponseChecked>(new OrderResponseChecked
                        {
                            result = 1,
                            return_product = products
                        }, new JsonMediaTypeFormatter(), "application/json");
                        return response;
                    }
                }
                else
                {

                    response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                    {
                        result = 0,
                        error = new Error()
                        {
                            message =
                                "You are not able to create return request because order has been not shipped/completed yet"
                        }
                    }, new JsonMediaTypeFormatter(), "application/json");
                    return response;

                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
                {
                    result = 0,
                    error = new Error()
                    {
                        message = "During creating return request error has occurred.",
                        description = "Please contact customer support."
                    }
                }, new JsonMediaTypeFormatter(), "application/json");
                return response;
            }
            response.Content = new ObjectContent<OrderResponseError>(new OrderResponseError
            {
                result = 0,
                error = new Error()
                {
                    message = "During creating return request error has occurred."
                }
            }, new JsonMediaTypeFormatter(), "application/json");
            return response;
        }

        [System.Web.Http.Route("api/orders/deleteOrder")]
        [System.Web.Http.HttpPost]
        public void DeleteOrder(DeleteOrderRequest values)
        {
            string path = HttpContext.Current.Server.MapPath("~/Labels/");
            foreach (var returnId in values.returns)
            {
                try
                {
                    var order = ReturnsRepository.GetByReturnId(returnId);
                    if ((File.Exists(Path.Combine(path, order.ShippingLabel + ".gif"))))
                    {
                        File.Delete(Path.Combine(path, order.ShippingLabel + ".gif"));
                    }
                    ReturnsRepository.Remove(returnId);

                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    Logger.Error(e.ToString(), "Shipping label saving");
                }
            }
        }

        [System.Web.Http.Route("BigCommerce/api/orders/updateStatus")]
        [System.Web.Http.HttpPost]
        public ActionResult UpdateStatus(StatusUpdateRequest value)
        {
            string status = "Approved";
            try
            {
                if (value.returnStatus.Contains("2"))
                    ReturnsRepository.CompleteReturn(value.returnId);
                else
                {
                    if (value.returnStatus.Contains("3"))
                    {
                        status = "Canceled";
                    }
                    ReturnsRepository.UpdateStatus(value.returnId, status);
                }

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new HttpStatusCodeResult(404);
            }
            return new HttpStatusCodeResult(200);
        }

        [System.Web.Http.Route("BigCommerce/api/orders/updateNotes")]
        [System.Web.Http.HttpPost]
        public ActionResult UpdateNotes(NoteUpdateRequest value)
        {
            try
            {
                if (value.returnNotes == null)
                {
                    value.returnNotes = "";
                }
                ReturnsRepository.UpdateNotes(value.returnId, value.returnNotes);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new HttpStatusCodeResult(404);
            }
            return new HttpStatusCodeResult(200);
        }

    }
}

