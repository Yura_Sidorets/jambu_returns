﻿using System.Net.Http.Formatting;
using System.Web.Http;
using bigcommerce_jambu_returns.Properties;
using bigcommerce_returns.Data;
using bigcommerce_returns.Repositories;
using Ninject;

namespace bigcommerce_jambu_returns.Controllers
{
    public class ConfigController : ApiController
    {
        [Inject]
        public IMerchantRepository MerchantRepository { get; set; }
        [Inject]
        public IConfigurationRepository ConfigRepository { get; set; }

        [Route("api/config/update")]
        public void Post(FormDataCollection values)
        {
            var signedPayload = values.Get("payload");
            var store = StoreProvider.ReadStore(signedPayload, Settings.Default.ClientSecret);

            var config = ConfigRepository.GetApplicationConfiguration(int.Parse(store.Owner.Id));

            if (config.Address == null || config.UpsCredentials == null || config.FtpCredentials == null)
            {
                ConfigRepository.SaveApplicationConfiguration(int.Parse(store.Owner.Id), new ApplicationConfiguration
                {
                    UpsCredentials = new UpsCredentials
                    {
                        AccessLicenseNumber = values.Get("access"),
                        AccountNumber = values.Get("account"),
                        Email = values.Get("email"),
                        Password = values.Get("pass")
                    },
                    Address = new Address
                    {
                        AccountName = values.Get("name"),
                        Address1 = values.Get("address1"),
                        Address2 = values.Get("address2"),
                        Country = values.Get("country"),
                        City = values.Get("city"),
                        State = values.Get("state"),
                        Telephone = values.Get("telephone"),
                        Zip = values.Get("zip")
                    },
                   FtpCredentials = new FtpCredentials()
                   {
                       FtpUsername = values.Get("ftp_username"),
                       FtpPassword = values.Get("ftp_password"),
                       FtpPath = values.Get("ftp_path"),
                       FtpUrl = values.Get("ftp_url")
                   }
                });
            }
            else
            {
                ConfigRepository.UpdateApplicationConfiguration(int.Parse(store.Owner.Id), new ApplicationConfiguration
                {
                    UpsCredentials = new UpsCredentials
                    {
                        AccessLicenseNumber = values.Get("access"),
                        AccountNumber = values.Get("account"),
                        Email = values.Get("email"),
                        Password = values.Get("pass")
                    },
                    Address = new Address
                    {
                        AccountName = values.Get("name"),
                        Address1 = values.Get("address1"),
                        Address2 = values.Get("address2"),
                        Country = values.Get("country"),
                        City = values.Get("city"),
                        State = values.Get("state"),
                        Telephone = values.Get("telephone"),
                        Zip = values.Get("zip")
                    },
                    FtpCredentials = new FtpCredentials()
                    {
                        FtpUsername = values.Get("ftp_username"),
                        FtpPassword = values.Get("ftp_password"),
                        FtpPath = values.Get("ftp_path"),
                        FtpUrl = values.Get("ftp_url")
                    }
                });
            }
           
        }
    }
}
