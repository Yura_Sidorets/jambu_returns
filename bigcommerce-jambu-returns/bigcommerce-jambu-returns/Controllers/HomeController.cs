﻿using System.Web.Mvc;

namespace bigcommerce_jambu_returns.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            
            return View();
        }
    }
}
