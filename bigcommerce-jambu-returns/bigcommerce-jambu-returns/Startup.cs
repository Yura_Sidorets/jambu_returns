﻿using System.Configuration;
using bigcommerce_jambu_returns.Utils;
using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(bigcommerce_jambu_returns.Startup))]

namespace bigcommerce_jambu_returns
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage(ConfigurationManager.ConnectionStrings["ReturnsConectionString"].ConnectionString);

            app.UseHangfireDashboard();
            app.UseHangfireServer();
            RecurringJob.AddOrUpdate(() => ReportHelper.GenerateReport(ConfigurationManager.ConnectionStrings["ReturnsConectionString"].ConnectionString), Cron.Daily);
            RecurringJob.AddOrUpdate(() => OlapicHelper.PostFeed(ConfigurationManager.ConnectionStrings["ReturnsConectionString"].ConnectionString), Cron.Daily);
        }
    }
}
