﻿using System;
using System.Diagnostics;

namespace bigcommerce_jambu_returns
{
    public  class Logger : ILogger
    {
        private TraceSource _source;
        public Logger()
        {
        }
        public  void Error(string message, string module)
        {
            WriteEntry(message, "error", module);
        }

        public  void Warning(string message, string module)
        {
            WriteEntry(message, "warning", module);
        }

        public  void Info(string message, string module)
        {
            WriteEntry(message, "info", module);
        }

        private static void WriteEntry(string message, string type, string module)
        {
            Trace.WriteLine(
                    string.Format("{0},{1},{2},{3}",
                                  DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                  type,
                                  module,
                                  message));
        }
    }
}