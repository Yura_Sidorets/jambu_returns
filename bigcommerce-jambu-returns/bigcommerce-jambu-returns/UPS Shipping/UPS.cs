﻿using System;
using System.Net;
using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.UPS_Shipping
{
    public class UPS
    {
        public static PrShipmentResponse RequestShipment(PrShipmentRequest request)
        {
            //  "https://wwwcie.ups.com/rest/Ship"; //test address 
            string uri = "https://onlinetools.ups.com/rest/Ship";

            string json = JsonConvert.SerializeObject(request);
            string result = "";
            using (var client = new WebClient())
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                result = client.UploadString(uri, "POST", json);
                if (result.Contains("Fault"))
                {
                    var res = JsonConvert.DeserializeObject<FaultShipment>(result);
                    throw new Exception(res.Fault.detail.Errors.ErrorDetail.PrimaryErrorCode.Description);
                }
            }

            return JsonConvert.DeserializeObject<PrShipmentResponse>(result);
        }

        public static LabelRecoveryResponse RecoverLabel(LabelRecoveryRequest request)
        {
            //https://wwwcie.ups.com/rest/LBRecovery //test address
            string uri = "https://onlinetools.ups.com/rest/LBRecovery";

            string json = JsonConvert.SerializeObject(request);
            string result = "";
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                result = client.UploadString(uri, "POST", json);
                if (result.Contains("Fault"))
                {
                    var res = JsonConvert.DeserializeObject<FaultShipment>(result);
                    throw new Exception(res.Fault.detail.Errors.ErrorDetail.PrimaryErrorCode.Description);
                }
            }

            return JsonConvert.DeserializeObject<LabelRecoveryResponse>(result);
        }
    }
}