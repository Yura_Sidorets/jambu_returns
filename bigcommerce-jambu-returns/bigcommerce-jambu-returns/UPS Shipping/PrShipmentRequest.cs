﻿using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.UPS_Shipping
{
    public class UsernameToken
    {

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }
    }

    public class ServiceAccessToken
    {

        [JsonProperty("AccessLicenseNumber")]
        public string AccessLicenseNumber { get; set; }
    }

    public class UPSSecurity
    {

        [JsonProperty("UsernameToken")]
        public UsernameToken UsernameToken { get; set; }

        [JsonProperty("ServiceAccessToken")]
        public ServiceAccessToken ServiceAccessToken { get; set; }
    }

    public class TransactionReference
    {

        [JsonProperty("CustomerContext")]
        public string CustomerContext { get; set; }
    }

    public class Request
    {

        [JsonProperty("RequestOption")]
        public string RequestOption { get; set; }

        [JsonProperty("TransactionReference")]
        public TransactionReference TransactionReference { get; set; }
    }

    public class Phone
    {

        [JsonProperty("Number")]
        public string Number { get; set; }

        [JsonProperty("Extension")]
        public string Extension { get; set; }
    }

    public class Address
    {

        [JsonProperty("AddressLine")]
        public string AddressLine { get; set; }

        [JsonProperty("City")]
        public string City { get; set; }

        [JsonProperty("StateProvinceCode")]
        public string StateProvinceCode { get; set; }

        [JsonProperty("PostalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("CountryCode")]
        public string CountryCode { get; set; }
    }

    public class Shipper
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("AttentionName")]
        public string AttentionName { get; set; }

        [JsonProperty("TaxIdentificationNumber")]
        public string TaxIdentificationNumber { get; set; }

        [JsonProperty("Phone")]
        public Phone Phone { get; set; }

        [JsonProperty("ShipperNumber")]
        public string ShipperNumber { get; set; }

        [JsonProperty("FaxNumber")]
        public string FaxNumber { get; set; }

        [JsonProperty("Address")]
        public Address Address { get; set; }
    }

    public class ShipTo
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("AttentionName")]
        public string AttentionName { get; set; }

        [JsonProperty("Phone")]
        public Phone Phone { get; set; }

        [JsonProperty("Address")]
        public Address Address { get; set; }

    }

    public class ShipFrom
    {

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("AttentionName")]
        public string AttentionName { get; set; }

        [JsonProperty("Phone")]
        public Phone Phone { get; set; }

        [JsonProperty("FaxNumber")]
        public string FaxNumber { get; set; }

        [JsonProperty("Address")]
        public Address Address { get; set; }
    }

    public class BillShipper
    {

        [JsonProperty("AccountNumber")]
        public string AccountNumber { get; set; }
    }

    public class ShipmentCharge
    {

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("BillShipper")]
        public BillShipper BillShipper { get; set; }
    }

    public class PaymentInformation
    {

        [JsonProperty("ShipmentCharge")]
        public ShipmentCharge ShipmentCharge { get; set; }
    }

    public class Service
    {

        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }

    public class Packaging
    {

        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }

    public class UnitOfMeasurement
    {

        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }

    public class Dimensions
    {

        [JsonProperty("UnitOfMeasurement")]
        public UnitOfMeasurement UnitOfMeasurement { get; set; }

        [JsonProperty("Length")]
        public string Length { get; set; }

        [JsonProperty("Width")]
        public string Width { get; set; }

        [JsonProperty("Height")]
        public string Height { get; set; }
    }

    public class PackageWeight
    {

        [JsonProperty("UnitOfMeasurement")]
        public UnitOfMeasurement UnitOfMeasurement { get; set; }

        [JsonProperty("Weight")]
        public string Weight { get; set; }
    }

    public class Package
    {

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Packaging")]
        public Packaging Packaging { get; set; }

        [JsonProperty("Dimensions")]
        public Dimensions Dimensions { get; set; }

        [JsonProperty("PackageWeight")]
        public PackageWeight PackageWeight { get; set; }

        [JsonProperty("ReferenceNumber")]
        public ReferenceNumber ReferenceNumber { get; set; }
    }

    public class ReferenceNumber
    {
        public string BarCodeIndicator { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }

    public class Shipment
    {

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Shipper")]
        public Shipper Shipper { get; set; }

        [JsonProperty("ShipTo")]
        public ShipTo ShipTo { get; set; }

        [JsonProperty("ShipFrom")]
        public ShipFrom ShipFrom { get; set; }

        [JsonProperty("PaymentInformation")]
        public PaymentInformation PaymentInformation { get; set; }

        [JsonProperty("Service")]
        public Service Service { get; set; }

        [JsonProperty("Package")]
        public Package Package { get; set; }
    }

    public class LabelImageFormat
    {

        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }

    public class LabelSpecification
    {

        [JsonProperty("LabelImageFormat")]
        public LabelImageFormat LabelImageFormat { get; set; }

        [JsonProperty("HTTPUserAgent")]
        public string HTTPUserAgent { get; set; }
    }

    public class ShipmentRequest
    {

        [JsonProperty("Request")]
        public Request Request { get; set; }

        [JsonProperty("Shipment")]
        public Shipment Shipment { get; set; }

        [JsonProperty("LabelSpecification")]
        public LabelSpecification LabelSpecification { get; set; }
    }

    public class PrShipmentRequest
    {

        [JsonProperty("UPSSecurity")]
        public UPSSecurity UPSSecurity { get; set; }

        [JsonProperty("ShipmentRequest")]
        public ShipmentRequest ShipmentRequest { get; set; }
    }


}