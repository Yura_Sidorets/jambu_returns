﻿using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.UPS_Shipping
{
   

    public class LabelImage
    {

        [JsonProperty("LabelImageFormat")]
        public LabelImageFormat LabelImageFormat { get; set; }

        [JsonProperty("GraphicImage")]
        public string GraphicImage { get; set; }
    }

    public class ImageFormat
    {

        [JsonProperty("Code")]
        public string Code { get; set; }
    }

    public class Image
    {

        [JsonProperty("ImageFormat")]
        public ImageFormat ImageFormat { get; set; }

        [JsonProperty("GraphicImage")]
        public string GraphicImage { get; set; }
    }

    public class Receipt
    {

        [JsonProperty("Image")]
        public Image Image { get; set; }
    }

    public class LabelResults
    {

        [JsonProperty("TrackingNumber")]
        public string TrackingNumber { get; set; }

        [JsonProperty("LabelImage")]
        public LabelImage LabelImage { get; set; }

        [JsonProperty("Receipt")]
        public Receipt Receipt { get; set; }
    }

    public class LabelRecoveryRespons
    {

        [JsonProperty("Response")]
        public Response Response { get; set; }

        [JsonProperty("LabelResults")]
        public LabelResults LabelResults { get; set; }
    }

    public class LabelRecoveryResponse
    {

        [JsonProperty("LabelRecoveryResponse")]
        public LabelRecoveryRespons LabelRecoveryRespons { get; set; }
    }


}