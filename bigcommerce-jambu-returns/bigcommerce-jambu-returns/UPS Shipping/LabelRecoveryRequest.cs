﻿using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.UPS_Shipping
{

    public class Translate
    {

        [JsonProperty("LanguageCode")]
        public string LanguageCode { get; set; }

        [JsonProperty("DialectCode")]
        public string DialectCode { get; set; }

        [JsonProperty("Code")]
        public string Code { get; set; }
    }

    public class LabelRecover
    {

        [JsonProperty("LabelSpecification")]
        public LabelSpecification LabelSpecification { get; set; }

        [JsonProperty("Translate")]
        public Translate Translate { get; set; }

        [JsonProperty("TrackingNumber")]
        public string TrackingNumber { get; set; }
    }

    public class LabelRecoveryRequest
    {

        [JsonProperty("UPSSecurity")]
        public UPSSecurity UPSSecurity { get; set; }

        [JsonProperty("LabelRecoveryRequest")]
        public LabelRecover LabelRecover{ get; set; }
    }


}