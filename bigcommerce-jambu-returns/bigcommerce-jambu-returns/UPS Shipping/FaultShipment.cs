﻿using Newtonsoft.Json;

namespace bigcommerce_jambu_returns.UPS_Shipping
{
    public class PrimaryErrorCode
    {

        [JsonProperty("Code")]
        public string Code { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }

    public class ErrorDetail
    {

        [JsonProperty("Severity")]
        public string Severity { get; set; }

        [JsonProperty("PrimaryErrorCode")]
        public PrimaryErrorCode PrimaryErrorCode { get; set; }
    }

    public class Errors
    {

        [JsonProperty("ErrorDetail")]
        public ErrorDetail ErrorDetail { get; set; }
    }

    public class Detail
    {

        [JsonProperty("Errors")]
        public Errors Errors { get; set; }
    }

    public class Fault
    {

        [JsonProperty("faultcode")]
        public string faultcode { get; set; }

        [JsonProperty("faultstring")]
        public string faultstring { get; set; }

        [JsonProperty("detail")]
        public Detail detail { get; set; }
    }

    public class FaultShipment
    {

        [JsonProperty("Fault")]
        public Fault Fault { get; set; }
    }


}