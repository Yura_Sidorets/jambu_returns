﻿using System.Security.Cryptography;
using System.Text;

namespace bigcommerce_jambu_returns.Cryptography
{
    public class HMACCryptography
    {
        public static string Decode(string key, string message)
        {
            var keyByte = Encoding.UTF8.GetBytes(key);

            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                hmacsha256.ComputeHash(Encoding.UTF8.GetBytes(message));

                return ByteToString(hmacsha256.Hash);
            }
        }

        private static string ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString("X2"); /* hex format */
            return sbinary;
        }
    }
}