﻿using System.Collections.Generic;
using bigcommerce_returns.Data;
using PagedList;

namespace bigcommerce_jambu_returns.ViewModels
{
    public class ReturnViewModel
    {
        public List<Return> ReturnOrders { get; set; }
        public List<int> Products { get; set; }
        public string SearchString { get; set; }

        public PagedList<int> PagedProducts { get; set; }

        public ReturnViewModel()
        {
            ReturnOrders = new List<Return>();
            Products = new List<int>();
        }
    }
}